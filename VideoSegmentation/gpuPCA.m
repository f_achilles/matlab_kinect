function [PCcoeff, PCvec] = gpuPCA(indices,dataNx,dataNy,dataNz)
% checks if dataNx3 is available on the GPU and then executes PCA
% computation
if existsOnGPU(dataNx3)
    [PCvec, PCcoeff] = eig(cov(dataNx3));
    PCcoeff = diag(PCcoeff);
    [PCcoeff,perm] = sort(PCcoeff,'descend');
    PCvec = PCvec(:,perm);
else
    error('input data is not available on GPU')
end
end