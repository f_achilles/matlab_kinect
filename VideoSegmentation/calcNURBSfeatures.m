%% load an oversegmented dataset
% input is named "clusterContainer", type uint16
load('clusterContainerChrisHalfBlanket')
W=size(clusterContainer,2);
H=size(clusterContainer,1);
numFrames=size(clusterContainer,3);

% load the corresponding depth data
load('ChrisHalfBlanket100frames')
% make index variables for fast Pointcloud Conversion
[widthIndex, heightIndex] = meshgrid(1:W,1:H);



%% set initial NURBS parameters
% control points
numCtrlPtsPerRow = 5;
% knots
numKnotsPerRow = 5;


initialClusterFrame = clusterContainer(:,:,1);
initialClusterFrame=initialClusterFrame(:);
initialClusterIndices = unique(initialClusterFrame(initialClusterFrame~=0));

%% start matching loop
for frameInd = 1:1%numFrames
    % convert depth frame to Pointcloud
    pc=zeros([H W 3],'single');
    D=ChrisHalfBlanket100frames(:,:,frameInd);
    f=5.453;
    pc(:,:,3) = -single(D);
    pc(:,:,1) = pc(:,:,3)/f.*((widthIndex-W/2)*0.0093+0.063);
    pc(:,:,2) = pc(:,:,3)/f.*((heightIndex-H/2)*0.0093+0.039);
    X=pc(:,:,1);
    Y=pc(:,:,2);
    Z=pc(:,:,3);
    Z(Z==0)=NaN;
    Z=Z(:);
    validIndices=find(~isnan(Z));
    X=X(validIndices);
    Y=Y(validIndices);
    Z=Z(validIndices);
    
    c_clusterFrame = clusterContainer(:,:,frameInd);
    c_clusterFrame = c_clusterFrame(validIndices);
    for clusterInd = initialClusterIndices(1)
        c_cluster = find(c_clusterFrame==clusterInd);
        % calculate dimensions
        xmin = min(X(c_cluster));
        xmax = max(X(c_cluster));
        ymin = min(Y(c_cluster));
        ymax = max(Y(c_cluster));
        zmin = min(Z(c_cluster));
        zmax = max(Z(c_cluster));
        % init NURBS
        [xCtrl,yCtrl]=meshgrid(linspace(xmin,xmax,numCtrlPtsPerRow),linspace(ymin,ymax,numCtrlPtsPerRow));
        zCtrl = (zmin+zmax)*ones(numCtrlPtsPerRow)/2;
        ptsCtrl = permute(cat(3,xCtrl,yCtrl,zCtrl),[3 2 1]);
        knotVector = meshgrid(linspace(0,1,numKnotsPerRow));
        knots = {[0 0 0 1/3 2/3 1 1 1], [0 0 0 1/3 2/3 1 1 1]};
        c_nurbs = nrbmak(ptsCtrl,knots);
%         % control plot
%         figure
%         plot3(X(c_cluster),Y(c_cluster),Z(c_cluster),'*')
%         hold on
%         nrbplot(c_nurbs, [20 20]);
%         hold off
%         axis vis3d image
        
        % clear u-v-coordinates of former patches
%         clear functions

        % reshape ptCloud points to match the NURBS scheme
%         clusterPointsReshaped = reshape([X(c_cluster) Y(c_cluster) Z(c_cluster)]',)
        
        % optimize NURBS
        fun = @(ctrl)NURBSdistanceForLmSolver(ctrl,[X(c_cluster) Y(c_cluster) Z(c_cluster)],c_nurbs);
        
        options = optimset('fminsearch');
        options.Display = 'iter';
        
        % 1)choose optimizer
        zOfCtrlPts = fminsearchbnd(fun, double(zCtrl(1)*ones(numCtrlPtsPerRow^2,1)), -5000*ones(numCtrlPtsPerRow^2,1), 0*ones(numCtrlPtsPerRow^2,1));
        % 2)choose objective function
        %%% inside the objective function, one needs to find the surface
        %%% coordinates that are closest to the current point. Opt1: find
        %%% all at once (multivariate optimization), Opt2: find one after
        %%% the other, Opt3: do Opt2, but start with the solution of the
        %%% previous point if the PTcloud points are neighbors
        
    end
end

%   coefs = cat(3,[0 0; 0 1],[1 1; 0 1]); 
%   knots = {[0 0 1 1]  [0 0 1 1]} ;
%   c_nurbs2 = nrbmak(coefs,knots);

















