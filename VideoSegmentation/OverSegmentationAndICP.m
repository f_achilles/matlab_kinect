%% load depth frame and over-segment it
% clear all
% load recorded depth stream
% dataDir='D:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings\allLabelledParts';
dataDir = 'C:\Projects\RangeVideoSegmentation';
matFiles = dir([dataDir '/*.mat']);
if numel(matFiles)~=0
    disp('Choose the recording to be replayed')
    for iFiles = 1:numel(matFiles)
        disp([num2str(iFiles) ': ' matFiles(iFiles).name])
    end
else
    error('no .mat files found in current folder')
end
indFile = input('Input valid number and press enter: ');
load([dataDir '/' matFiles(indFile).name])
varname=whos('-file',[dataDir '/' matFiles(indFile).name]);
varname=varname.name;
depthContainer = evalin('base',varname);
if ~strcmp(varname, 'depthContainer')
    clear(varname);
end
D = depthContainer(:,:,1);
W=size(D,2);
H=size(D,1);
% make index variables for fast Pointcloud Conversion
[widthIndex, heightIndex] = meshgrid(1:W,1:H);
% convert first depth frame to Pointcloud
pc=zeros([size(D) 3],'single');
f=5.453;
pc(:,:,3) = -single(D);
pc(:,:,1) = pc(:,:,3)/f.*((widthIndex-W/2)*0.0093+0.063);
pc(:,:,2) = pc(:,:,3)/f.*((heightIndex-H/2)*0.0093+0.039);
X=pc(:,:,1);
Y=pc(:,:,2);
Z=pc(:,:,3);
Z(Z==0)=NaN;

% remove annoying connections in surf-plot
% threshold is 20cm at the moment
diffZx = abs([diff(Z,1,2) zeros(H,1)]);
diffZy = abs([diff(Z,1,1); zeros(1,W)]);
edgeMask = diffZx>200 | diffZy>200;
edgeMask = imdilate(edgeMask,ones(3));

ZforSurfPlot = Z;
ZforSurfPlot(edgeMask)=NaN;

figure
patientSurface=surf(double(X),double(Y),double(ZforSurfPlot),'edgecolor','none','facecolor','interp');%,'alphadata',double(~edgeMask));
lighting gouraud
camlight
axis image
axis vis3d
xlabel('X axis')
ylabel('Y axis')
zlabel('Z axis')

clear ZforSurfPlot edgeMask diffZx diffZy

Z=Z(:);
validIndices=find(~isnan(Z));
X=X(validIndices);
Y=Y(validIndices);
Z=Z(validIndices);
numPts = numel(X);

% create search objects for points and normals
kdtreePtsOld = KDTreeSearcher([X Y Z]);
% compute normals
tic
normalsOfPts = calcNormalsWithPCA(kdtreePtsOld, [0;0;0]);
disp 'Time to compute the normals:'
toc
% pause
% 
% % visualization of the normals
% stepsize = floor(numPts/200);
% hold on
% for nInd = 1:stepsize:numPts
%     if ~isnan(normalsOfPts(nInd,1))
%         arrow3([X(nInd) Y(nInd) Z(nInd)],[X(nInd)+250*normalsOfPts(nInd,1) Y(nInd)+250*normalsOfPts(nInd,2) Z(nInd)+250*normalsOfPts(nInd,3)],'b-1')
%     end
% end
% axis tight
% hold off

% % color the surface with the normal angle with respect to the Viewpoint
% normalCosines = dot(normalsOfPts(:,1:2)',normalsOfPts(:,1:2)');
% normalCData = pc(:,:,1)*NaN;
% normalCData(validIndices) = normalCosines;
% set(patientSurface,'cdata',double(normalCData));

%%
% in a while loop: 1) assign all close neighbors to one family 2)
% throw out those that have a too different normal

% Distance threshold for cluster-radius (in [mm])
thrClust = 200;
thrAngleDeg = 20;
clusterCounter = 1;
% initialize clusterIndex vector
clusterIndexVec = zeros(numPts,1,'uint16');

while any(clusterIndexVec==0)
unsegmentedPixels = find(clusterIndexVec==0);
randomIndex = randi(numel(unsegmentedPixels));
seedIndex = unsegmentedPixels(randomIndex);

clusterIndices = rangesearch(kdtreePtsOld,[X(seedIndex) Y(seedIndex) Z(seedIndex)],thrClust);
if ~isempty(clusterIndices)
clusterIndices = clusterIndices{1};
end
if ~isempty(clusterIndices)
    % only look at pixels that were not yet clustered
    clusterIndices = clusterIndices(clusterIndexVec(clusterIndices)==0);
    if ~isempty(clusterIndices)
        % sort out those with a too different normal
        anglesDeg = 180*acos(dot(repmat(normalsOfPts(seedIndex,:)',1,numel(clusterIndices)),normalsOfPts(clusterIndices,:)'))/pi;
        clusterIndices = clusterIndices(anglesDeg<thrAngleDeg);
        if ~isempty(clusterIndices)
            % enter cluster in clusterIndex vector
            clusterIndexVec(clusterIndices)=clusterCounter;
        end
    end
end
clusterIndexVec(seedIndex)=clusterCounter;
clusterCounter = clusterCounter+1;
end
clusterCounter = clusterCounter-1;
% plot the colouring of the clusters
clusterCData = pc(:,:,1)*NaN;
clusterCData(validIndices) = clusterIndexVec;
set(patientSurface,'cdata',double(clusterCData),'facecolor','flat');
colormap(colorcube(clusterCounter))
caxis auto

figure
colormap(colorcube(clusterCounter))
imagesc(clusterCData)
axis image
set(gca,'xdir','reverse')

%% Voronoi labelling
% we now have an oversegmentation that looks good, showing circular regions
% that are grouped due to closeness and showing similar surface normals.
%
% However, it is hard to keep this labelling consistent if single objects
% move throughout the frames. This is why we impose a Voronoi labelling,
% where each pixel gets the label of its closest label region's center.

clusterCenters = zeros(clusterCounter,3);
for i=1:clusterCounter
    clusterIndices = find(clusterIndexVec==i);
    clusterCenters(i,:) = mean([X(clusterIndices) Y(clusterIndices) Z(clusterIndices)],1);
end
% build kdTree
kdTreeClusterCenters = KDTreeSearcher(clusterCenters);
% update labelling as Voronoi labelling
clusterIndexVecVoronoi=knnsearch(kdTreeClusterCenters,[X Y Z]);

%plot the result
clusterCDataVoronoi = pc(:,:,1)*NaN;
clusterCDataVoronoi(validIndices) = clusterIndexVecVoronoi;
figure
colormap(colorcube(clusterCounter))
voronoiImage = imagesc(clusterCDataVoronoi);
set(gca,'xdir','reverse')
axis image

set(patientSurface,'cdata',double(clusterCDataVoronoi),'facecolor','flat');
caxis auto

%% discard labels that describe too few points, discard NaNs
minimumPtsPerLabel = 100;
% count size of each label
uniqueClusterLabels=unique(clusterIndexVecVoronoi);
clusterSize=zeros(size(uniqueClusterLabels));
for i=1:numel(uniqueClusterLabels)
    binaryCompare = clusterIndexVecVoronoi==i;
    clusterSize(i) = sum(binaryCompare(:));
    if clusterSize(i) < minimumPtsPerLabel
        clusterIndexVecVoronoi(binaryCompare)=NaN;
    end
end
clusterFrameClean = pc(:,:,1)*NaN;
clusterFrameClean(validIndices) = clusterIndexVecVoronoi;

%% go through the whole video, match labels through ICP (winner takes all, NO new labels)
clusterContainer = zeros(size(depthContainer),'uint16');
clusterContainer(:,:,1)=uint16(clusterFrameClean);

numberOfTargetNeighbors = 5;
icpIterations=5;
% threshold distance for labelling the new pointcloud
labelThr = 50;
% disp 'Press key to start loop...'
drawnow
% pause


for frameInd = 2:100%size(depthContainer,3) % about 20secs per frame, so that 100 frames take 2000 secs, i.e. 40minutes
% convert depth frame to Pointcloud
D = depthContainer(:,:,frameInd);
pc=zeros([size(D) 3],'single');
W=size(D,2);
H=size(D,1);
f=5.453;

pc(:,:,3) = -single(D);
pc(:,:,1) = pc(:,:,3)/f.*((widthIndex-W/2)*0.0093+0.063);
pc(:,:,2) = pc(:,:,3)/f.*((heightIndex-H/2)*0.0093+0.039);

X=pc(:,:,1);
Y=pc(:,:,2);
Z=pc(:,:,3);
Z(Z==0)=NaN;
Z=Z(:);
validIndices=find(~isnan(Z));
X=X(validIndices);
Y=Y(validIndices);
Z=Z(validIndices);
numPts = numel(X);

% build kdTree
kdTreePtsNew = KDTreeSearcher([X Y Z]);

% match clusters of previous frame
uniqueClusterLabels=unique(clusterIndexVecVoronoi(~isnan(clusterIndexVecVoronoi)));
matchedPointcloudOld = kdtreePtsOld.X;
nearestTargetPts=cell(numel(uniqueClusterLabels),1);
rmsErrorToTargetPts=NaN*zeros(numel(uniqueClusterLabels),1);
for i=1:numel(uniqueClusterLabels)
    clusterIndices = find(clusterIndexVecVoronoi==uniqueClusterLabels(i));
    if ~isempty(clusterIndices)
        % calc center
        clusterCenter = mean(kdtreePtsOld.X(clusterIndices,:),1);
        % limit region for ICP
        targetRegion = rangesearch(kdTreePtsNew,clusterCenter,thrClust);
        targetRegion=targetRegion{1};
        if ~isempty(targetRegion)
            [R,T,Err]=icp((kdTreePtsNew.X(targetRegion,:))',(kdtreePtsOld.X(clusterIndices,:))',icpIterations,'Matching','kDtree');
            rmsErrorToTargetPts(i)=Err(end);
            matchedPointcloudOld(clusterIndices,:)=kdtreePtsOld.X(clusterIndices,:)*R'+repmat(T',numel(clusterIndices),1);
        end
    end
end
% assign to each new XYZ the label of the matched old point that is closest
% to it
% build kdTree of matched points
kdtreePtsOld = KDTreeSearcher(matchedPointcloudOld);
% update labelling
[NNIndex, labelDists]=knnsearch(kdtreePtsOld,[X Y Z]);
clusterIndexVecVoronoi = clusterIndexVecVoronoi(NNIndex);
clusterIndexVecVoronoi(labelDists>labelThr) = NaN;

% store labelled image
clusterFrameClean = pc(:,:,1)*NaN;
clusterFrameClean(validIndices) = clusterIndexVecVoronoi;
clusterContainer(:,:,frameInd)=uint16(clusterFrameClean);

kdtreePtsOld=kdTreePtsNew;
end



