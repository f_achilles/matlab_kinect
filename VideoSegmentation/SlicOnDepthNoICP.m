% Perform SLIC on each depth frame independently

% calculate kNN, edge and shape-based features for each SP in frame t and
% t+1
%
% connect Superpixels that are close and similar to one another
%
% mark SPs as occluded that did not find a match

% run the import script with user input
sourceFolder = './data';
depthContainer = loadMat(sourceFolder);

numFrames = size(depthContainer,3);

spContainer = uint16(depthContainer*0);
% perform SLIC on first frame
parfor frameInd=1:numFrames
depthImage = double(depthContainer(:,:,frameInd));
depthImage = depthImage/max(depthImage(:)); %all values should be inside [0,1]
depthImage = cat(3,depthImage,depthImage,depthImage);
tic
[labelledFrame] = slic(depthImage, 100, 5, 1.5);
toc
spContainer(:,:,frameInd)=labelledFrame;
end

% save('SLIC_EachFrameTest','spContainer')
% load SLIC_EachFrameTest
% numFrames = size(spContainer,3);

%% plot
% figure; colormap(hot(100));
% for indFrame = 1:numFrames
% image(spContainer(:,:,indFrame))
% axis image
% pause
% end

%% calc features for each SP per frame
W=size(spContainer,2);
H=size(spContainer,1);
% make index variables for fast Pointcloud Conversion
[widthIndex, heightIndex] = meshgrid(1:W,1:H);
% make pointcloud of first frame
f = 5.453;
spContainerNew = spContainer*0;
% self features
tic
for frameInd = 1:numFrames;
% tic % takes ~350ms
cSpFrame =spContainer(:,:,frameInd);
cDptFrame =depthContainer(:,:,frameInd);
uniqueSpLabels=unique(cSpFrame);
numLabels = numel(uniqueSpLabels);
featureContainerArray = zeros(numLabels,26);

Z = -single(depthContainer(:,:,frameInd));
X = Z.*((widthIndex-W/2)*0.0093+0.063)/f;
Y = Z.*((heightIndex-H/2)*0.0093+0.039)/f;
Z=Z(:);
X=X(:);
Y=Y(:);

for spInd = 1:numLabels
    cSpLabel = uniqueSpLabels(spInd);
    binaryLabelImage = cSpLabel==cSpFrame;
    indicesOfCurrLabel = find(binaryLabelImage);
    expandedBinarySpFrame = zeros(H+2,W+2);
    expandedBinarySpFrame(2:(end-1),2:(end-1))=binaryLabelImage;
    expandedIndicesOfCurrLabelBorder=bwboundaries(expandedBinarySpFrame);
    expandedIndicesOfCurrLabelBorder=sub2ind([H+2 W+2],expandedIndicesOfCurrLabelBorder{1}(:,1),expandedIndicesOfCurrLabelBorder{1}(:,2));
    expandedDptFrame = zeros(H+2,W+2);
    expandedDptFrame(2:(end-1),2:(end-1))=cDptFrame;
    %feature 1: ratio_invalid_pixels
    ratioInvalPixels = sum(cDptFrame(indicesOfCurrLabel)==0)/numel(indicesOfCurrLabel);
    %feature 2: spCenter , SpNormal , VarianceInPCADirections
    spNormal = [0 0 0];
    variancePCA = [0 0 0];
    if ratioInvalPixels < 0.10 % todo parameter
        [eigVecs,XYZinEigSpace,eigValues,~,~,spCenter]=pca([X(indicesOfCurrLabel) Y(indicesOfCurrLabel) Z(indicesOfCurrLabel)]);
        spNormal = cross(eigVecs(1,:),eigVecs(2,:));
        variancePCA = eigValues';
    end
    %feature 3: edgeDifferences
    %direction of looking over the edge is important. We
    %define from inside to outside, e.g. out-in for each edge pixel
    %The distribution of edge differences is important. One should store
    %[max, min, rms].
    indices4Neighborhood = [(expandedIndicesOfCurrLabelBorder+H+2)...
                            (expandedIndicesOfCurrLabelBorder-1)...
                            (expandedIndicesOfCurrLabelBorder-H-2)...
                            (expandedIndicesOfCurrLabelBorder+1)];
    neighborsRightUpLeftDown = ~expandedBinarySpFrame(indices4Neighborhood);
    
    depthsOutside = zeros(size(indices4Neighborhood));
    depthsOutside(neighborsRightUpLeftDown) = expandedDptFrame(indices4Neighborhood(neighborsRightUpLeftDown));
    depthDifferences = depthsOutside...
        -repmat(expandedDptFrame(expandedIndicesOfCurrLabelBorder),1,4);
    depthDifferences = depthDifferences(neighborsRightUpLeftDown);
    
    numEdgeNeighbors = sum(sum(neighborsRightUpLeftDown));
    edgeRMS = sqrt(dot(depthDifferences,depthDifferences)/numEdgeNeighbors);
    maxDiff = max(depthDifferences);
    minDiff = min(depthDifferences);
    
    featureContainerArray(spInd,1:13) = [ratioInvalPixels spCenter spNormal variancePCA maxDiff minDiff edgeRMS];
end
% toc % takes ~350 ms
%% neighbor features
% tic % takes 100ms
%extract centers and build kdTree
centerKdTree = KDTreeSearcher(featureContainerArray(:,2:4));

% feature 4: neighbors
% todo: find a good length of the vector describing the neighbors
nnK = 11;
histlength = 13;
binBorders = linspace(0,pi,histlength+1);
for spInd = 1:numLabels
    % find knn
    [indNN, distNN]=knnsearch(centerKdTree,featureContainerArray(spInd,2:4),'K',nnK);
    angleHist = zeros(1,histlength);
    for NNind = 2:nnK
        radAngle = mod(real(acos(dot(featureContainerArray(spInd,5:7),featureContainerArray(indNN(NNind),5:7)))),pi);
        binaryBinLocation = radAngle >= binBorders & radAngle < binBorders;
        angleHist(binaryBinLocation) = angleHist(binaryBinLocation) + 1/(distNN(NNind)+1);
    end
    featureContainerArray(spInd,14:end) = angleHist;
end
% toc % takes 100ms
%% normalize the feature container (% TODO: at least over 2 subsequent frames)
minFeats = min(featureContainerArray,[],1);
maxFeats = max(featureContainerArray,[],1);
normFeatMatCurrFrame = featureContainerArray - repmat(minFeats,numLabels,1);
AmplMinMax = maxFeats-minFeats;
validDescrElems = find(AmplMinMax > 100*eps);
normFeatMatCurrFrame(:,validDescrElems) = normFeatMatCurrFrame(:,validDescrElems) ./ repmat(AmplMinMax(validDescrElems),numLabels,1);


%% assign mutually nearest Superpixels
if frameInd == 1
    % yolo
    uniqueSpLabelsPrevFrame = uniqueSpLabels;
else
    % find knn of old centers to new ones
    kNeighborhood = 9;
    [indNN, distNN]=knnsearch(centerKdTree,featureContainerArrayPrevFrame(:,2:4),'K',kNeighborhood);
    
    adjacencyForward = false(numLabels); %rows: rev frame, columns: curr frame
    adjacencyBackward = false(numLabels);
    % find forward correspondences
    for spIndPrev = 1:numLabels
        % SSD calculation
        descriptorDiff = normFeatMatCurrFrame(indNN(spIndPrev,:),:)-repmat(normFeatMatPrevFrame(spIndPrev,:),kNeighborhood,1);
        SSDvectorForward = sum(descriptorDiff.*descriptorDiff,2);
        [SSDdist,minDistInd] = min(SSDvectorForward);
        % TODO: implement a winner takes all scheme.
        adjacencyForward(spIndPrev,indNN(spIndPrev,minDistInd))=true;
    end
%     % find backward correspondences
%     for spIndCurr = 1:numLabels
%         % SSD calculation
%         descriptorDiff = normFeatMatPrevFrame-repmat(normFeatMatCurrFrame(spIndCurr,:),numLabels,1);
%         SSDvectorBackward = sum(descriptorDiff.*descriptorDiff,2);
%         [SSDdist,minDistInd] = min(SSDvectorBackward);
%         adjacencyBackward(minDistInd,spIndCurr)=true;
%     end
%     adjacencyMutual = adjacencyForward & adjacencyBackward;
    adjacencyMutual = adjacencyForward;
    [SPpairsPrevInd,SPpairsCurrInd] = find(adjacencyMutual);
    % update SP labels
    newFrame = cSpFrame*0;
    for 7 = 1:numel(SPpairsPrevInd)
        prevSpLabel = uniqueSpLabelsPrevFrame(SPpairsPrevInd(adjPairInd));
        newFrame(cSpFrame==uniqueSpLabels(SPpairsCurrInd(adjPairInd))) = prevSpLabel;
    end
    spContainerNew(:,:,frameInd) = newFrame;
end

featureContainerArrayPrevFrame = featureContainerArray;

% Think of convenient way to handle 0-labels, e.g. SPs that were not found
% as nearest neighbor by any previous SP
uniqueSpLabelsPrevFrame = uniqueSpLabels*0;
uniqueSpLabelsPrevFrame() = uniqueSpLabels(SPpairsCurrInd);

normFeatMatPrevFrame = normFeatMatCurrFrame;

% %% make test images displaying normalized similarity (SSD) between patch descriptors
% figure
% originalSPFig = subplot(1,2,1);
% title('Original Superpixel A')
% SSDSPFig = subplot(1,2,2);
% title('normalized SSD from A to each other Superpixel')
% for spIndA = 1:numLabels
%     % SSD calculation
%     descriptorDiff = normFeatMat-repmat(normFeatMat(spIndA,:),numLabels,1);
%     SSDvector = sum(descriptorDiff.*descriptorDiff,2);
%     SSDvector = SSDvector/sum(SSDvector);
%     % DotProduct calculation %TODO: This is not working well as it
%     % amplifies the effect of high descriptor values attracting all
%     % neighbor SPs
% %     DOTvector = normFeatMat * normFeatMat(spIndA,:)';
%     
%     subplot(originalSPFig)
%     cSpLabel = uniqueSpLabels(spIndA);
%     binaryLabelImage = cSpLabel==cSpFrame;
%     imagesc(binaryLabelImage)
%     axis image
%     
%     subplot(SSDSPFig)
%     SSDframe = zeros(size(cSpFrame));
%     for spInd = 1:numLabels
%         cSpLabel = uniqueSpLabels(spInd);
%         binaryLabelImage = cSpLabel==cSpFrame;
%         SSDframe(binaryLabelImage) = SSDvector(spInd);
% %         SSDframe(binaryLabelImage) = DOTvector(spInd);
%     end
%     imagesc(SSDframe)
%     axis image
%     
%     pause
% end

end
toc
%% plot
figure; colormap(hot(100));
for indFrame = 1:numFrames
image(spContainerNew(:,:,indFrame))
axis image
pause
end
    

%% old code

 


% save('SegmentationWithSLIC_ICP','clusterContainer')


