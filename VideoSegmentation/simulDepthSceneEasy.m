% Simulate moving object in depth sequence SIMPLE APPROACH!!!

W = 50; %px
H = 50; %px
numFrames = 30;

bgDistance = 2000; %mm

objDistance = 1500; %mm
objShortEdge = 10; %px
objLongEdge = 20; %px

motionAmplitude = 15; %px, sine motion from left to right to left again

depthContainer = zeros(H,W,numFrames,'uint16');

for indFrame = 1:numFrames
    depthFrame = ones(H,W,'uint16')*bgDistance;
    
    objCenter = round(W/2-motionAmplitude+1+motionAmplitude*(1-cos((indFrame-1)*2*pi/numFrames)));
    depthFrame((H/2-objLongEdge/2+1):(H/2+objLongEdge/2),(objCenter-objShortEdge/2+1):(objCenter+objShortEdge/2)) = objDistance;
        
    depthContainer(:,:,indFrame) = depthFrame;
end

figure; colormap(jet);
for indFrame = 1:numFrames
imagesc(depthContainer(:,:,indFrame))
axis image
pause(0.05)
end