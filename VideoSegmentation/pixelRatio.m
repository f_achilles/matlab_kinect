%% we try: How does the pixel-to-pixel ratio look like with respect to the first frame?

% run the import script with user input
sourceFolder = './data';
depthContainer = loadMat(sourceFolder);

numFrames = size(depthContainer,3);

% % with respect to the first frame
% ratioContainer = double(depthContainer);
% referenceFrame = ratioContainer(:,:,1);
% nonZeroIndices = referenceFrame~=0;
% parfor frameInd=1:numFrames
%     cFrame = ratioContainer(:,:,frameInd);
%     cFrame(nonZeroIndices) = cFrame(nonZeroIndices)./referenceFrame(nonZeroIndices);
%     ratioContainer(:,:,frameInd) = cFrame;
% end
% 
% makeAVIVideo

% with respect to the respective previous frame
depthContainer = double(depthContainer);
ratioContainer = depthContainer*0;

% referenceFrame = ratioContainer(:,:,1);
% nonZeroIndices = referenceFrame~=0;
for frameInd=2:numFrames
    cFrame = depthContainer(:,:,frameInd);
    prevFrame = depthContainer(:,:,frameInd-1);
    nonZeroIndices = prevFrame~=0;
    
    nullFrame = zeros(size(cFrame));
    
    nullFrame(nonZeroIndices) = cFrame(nonZeroIndices)./prevFrame(nonZeroIndices);
    ratioContainer(:,:,frameInd) = nullFrame;
end

makeAVIVideo

% 
% spContainer = uint16(depthContainer*0);
% % perform SLIC on all frames
% parfor frameInd=1:numFrames
% depthImage = double(depthContainer(:,:,frameInd));
% depthImage = depthImage/max(depthImage(:)); %all values should be inside [0,1]
% depthImage = cat(3,depthImage,depthImage,depthImage);
% tic
% [labelledFrame] = slic(depthImage, 100, 5, 1.5);
% toc
% spContainer(:,:,frameInd)=labelledFrame;
% end