% test slic on depth images

% run the import script with user input
sourceFolder = './data';
depthContainer = loadMat(sourceFolder);

numFrames = size(depthContainer,3);

% perform SLIC on first frame
frameInd=1;
depthImage = double(depthContainer(:,:,frameInd));
depthImage = depthImage/max(depthImage(:)); %all values should be inside [0,1]
depthImage = cat(3,depthImage,depthImage,depthImage);
tic
[l, Am, Sp, d] = slic(depthImage, 1000, 5, 1.5); % weight 5 gives good results on depth, however with radius it takes 12secs per frame, with radius 1.5 it takes 13.7 secs
toc
figure
imshow(drawregionboundaries(l,depthImage,[255 0 0]))
axis image
%     imagePlane = -depthImage(:,:,1);
%     imagePlane(imagePlane==0)=NaN;
%     figure, surf(imagePlane,'edgecolor','none')
disp('yolo')

frameInd=1;
W=size(depthImage,2);
H=size(depthImage,1);

clusterContainer = zeros(H,W,numFrames,'uint16');
clusterContainer(:,:,frameInd) = uint16(l);

% make index variables for fast Pointcloud Conversion
[widthIndex, heightIndex] = meshgrid(1:W,1:H);

% make pointcloud of first frame
pc=zeros(size(depthImage),'single');
f=5.453;
pc(:,:,3) = -single(depthContainer(:,:,frameInd));
pc(:,:,1) = pc(:,:,3)/f.*((widthIndex-W/2)*0.0093+0.063);
pc(:,:,2) = pc(:,:,3)/f.*((heightIndex-H/2)*0.0093+0.039);
X=pc(:,:,1);
Y=pc(:,:,2);
Z=pc(:,:,3);
Z(Z==0)=NaN;
Z=Z(:);
binaryZValid=~isnan(Z);
validIndicesFrameOne=find(binaryZValid);
X=X(:);
Y=Y(:);
%% convert SLIC-labelled image into clusters
uniqueClusterLabels=unique(l(:));
clusterCell = cell(numel(uniqueClusterLabels),1);
clusterLabels = zeros(numel(validIndicesFrameOne),1);
OriginalClusterLabels = clusterLabels;
offset = 0;
runningInd = 0;
for i = 1:numel(uniqueClusterLabels)
    indicesOfCurrCluster = find(uniqueClusterLabels(i)==l(:) & binaryZValid);
    if ~isempty(indicesOfCurrCluster)
        runningInd = runningInd + 1;
        clusterCell{runningInd} = [X(indicesOfCurrCluster) Y(indicesOfCurrCluster) Z(indicesOfCurrCluster)];
        OriginalClusterLabels((1:numel(indicesOfCurrCluster))+offset)=uniqueClusterLabels(i);
        clusterLabels((1:numel(indicesOfCurrCluster))+offset)=runningInd;
        offset=offset+numel(indicesOfCurrCluster); %in the end, 'offset' will have the same value as 'numel(validIndices)'
    end
end
% get rid of clusterLabels that only had NaNs in them
uniqueClusterLabels = unique(clusterLabels);
uniqueClusterLabels = uniqueClusterLabels(uniqueClusterLabels~=0);
numOfValidClusters = numel(uniqueClusterLabels);
%%
% hard threshold to make ICP faster as it generates a kdTree for each
% target de nuevo ==> this needs to change, e.g. if the ICP gets a kdTree
% as input it uses it. Hence this parameter will vanish.
thrClust = 200; % 200mm = 20cm radius

% scalable for more precision at higher runtime
% #todo: don't forget to eval this threshold as one algorithm-parameter
icpIterations = 5;

% now pass on each cluster to the next frame via ICP
for frameInd = 2:numFrames
    % init vectors
    rmsErrorToTargetPts=zeros(numOfValidClusters,1);
    clusterCellNew=clusterCell(1:numOfValidClusters);
    
    % convert depth frame to Pointcloud
    pc(:,:,3) = -single(depthContainer(:,:,frameInd));
    pc(:,:,1) = pc(:,:,3)/f.*((widthIndex-W/2)*0.0093+0.063);
    pc(:,:,2) = pc(:,:,3)/f.*((heightIndex-H/2)*0.0093+0.039);
    X=pc(:,:,1);
    Y=pc(:,:,2);
    Z=pc(:,:,3);
    Z(Z==0)=NaN;
    Z=Z(:);
    validIndices=find(~isnan(Z));
    X=X(validIndices);
    Y=Y(validIndices);
    Z=Z(validIndices);
    numPts = numel(X);
    % build kdTree
    kdTreePtsNew = KDTreeSearcher([X Y Z]);
    
    R = cell(numOfValidClusters,1);
    T = cell(numOfValidClusters,1);

    tic
    % match clusters of previous frame to new frame
    parfor i=1:numOfValidClusters
        % calc center
        clusterCenter = mean(clusterCell{i},1);
        % limit region for ICP
        targetRegion = rangesearch(kdTreePtsNew,clusterCenter,thrClust);
        targetRegion=targetRegion{1};
        % perform ICP
        if ~isempty(targetRegion) % can be empty in case the cluster is now occluded
            [R{i},T{i},Err]=icp((kdTreePtsNew.X(targetRegion,:))',clusterCell{i}',icpIterations,'Matching','kDtree');
            rmsErrorToTargetPts(i)=Err(end);
            %TODO: reject some ICP which do not make sense (thresholds on R, T, and Err)
            % but without parameters it is better! i.e. in the end compare
            % via nearest neighbor (=the solution to all problems) and
            % accept only the best fits, setting back the other clusters to
            % their previous state stored in 'clusterCell{}'
            clusterCellNew{i}=clusterCell{i}*R{i}'+repmat(T{i}',size(clusterCell{i},1),1);
        end
    end
    toc
    
    clusterPointcloud=cell2mat(clusterCellNew);
    
%     % show depth image after ICP-moving the clusters around
%     newX = clusterPointcloud(:,1);
%     newY = clusterPointcloud(:,2);
%     newZ = clusterPointcloud(:,3);
%     
%     pixelInd_width = round(W/2+(newX./newZ)*(f/0.0093)-0.063/0.0093);
%     pixelInd_height = round(H/2+(newY./newZ)*(f/0.0093)-0.039/0.0093);
%     
%     validPixels = pixelInd_width>1 & pixelInd_width<W & pixelInd_height>1 & pixelInd_height<H;
%     pixelInd_width = pixelInd_width(validPixels);
%     pixelInd_height = pixelInd_height(validPixels);
%     newZ = -newZ(validPixels);
%     
%     linearIndices = sub2ind([H W],pixelInd_height,pixelInd_width);
%     depthFrame = NaN*widthIndex;
%     depthFrame(linearIndices) = newZ;
%     colormap(gray)
%     imagesc(depthFrame)
%     axis image
%     
%     XFrame = NaN*widthIndex;
%     XFrame(linearIndices) = newX(validPixels);
%     YFrame = NaN*widthIndex;
%     YFrame(linearIndices) = newY(validPixels);
%     figure
%     surf(XFrame,YFrame,-depthFrame,'edgecolor','none','facecolor','interp')
%     lighting gouraud
%     camlight
%     axis vis3d image
%     
    
    % assign labels to datapoints and solve assignment conflicts
    % for each clustered point, find nearest neighbor in current frame,
    % store distance
    [NNIndexArray, labelDistsArray]=knnsearch(kdTreePtsNew.X,clusterPointcloud);
    % identify all found targets and the indices of the points that found
    % them
    [uniqueTargetPoints,~,reconstructionIndices]=unique(NNIndexArray); % uniqueTargetPoints(reconstructionIndices)=NNIndexArray
    
    labellingVector = zeros(numel(uniqueTargetPoints),1,'single');
    % avoiding the for-loop would result in a too-huge matrix (numTargets x
    % numClusters), about 1GB big at 250.000 target points
    tic
    parfor i = 1:numel(uniqueTargetPoints)
        conflictingClusters = clusterLabels(reconstructionIndices==i); % find all the occurrences of unique target point number i, assign to clusters that want it
        if numel(conflictingClusters) > 1
            % catch the case when we have several source patch points, but
            % they all belong to the same patch
            [uniqueClusterInd]=unique(conflictingClusters);
            if numel(uniqueClusterInd)==1
                labellingVector(i) = uniqueClusterInd;
            else
                mismatchTerm = zeros(numel(conflictingClusters),1);
                NNdistancesOfPtOfConflictingClusters = labelDistsArray(reconstructionIndices==i);
                for j=1:numel(conflictingClusters)
                    clusterInd = conflictingClusters(j);
        %             mismatchTerm(j) = R{clusterInd}*T{clusterInd}*rmsErrorToTargetPts(clusterInd)*NNdistancesOfPtOfConflictingClusters(j);
                    mismatchTerm(j) = rmsErrorToTargetPts(clusterInd)*NNdistancesOfPtOfConflictingClusters(j);
                end

                [~,bestClusterIndInd]=min(mismatchTerm);
                bestClusterInd = conflictingClusters(bestClusterIndInd);

                labellingVector(i)=bestClusterInd;
            end
        elseif numel(conflictingClusters)== 1
            labellingVector(i) = conflictingClusters;
        end
    end
    toc
    
    % if a cluster did not get any target point, it was occluded somehow
    % and its position will be frozen
    % TODO: however it would be cooler if this patch would be controlled by
    % its neighbors now. Then again, this only helps as soon as the
    % spawning of new superpixels was implemented.
    uniqueLabels=unique(labellingVector);
    parfor i=1:numOfValidClusters
        if ~any(uniqueLabels==i)
            clusterCellNew{i}=clusterCell{i};
        end
    end
    clusterCell = clusterCellNew;
    
    % store labelled image
    labelledFrame = widthIndex*NaN;
    labelledFrame(validIndices(uniqueTargetPoints)) = labellingVector;
%     colormap(gray)
%     imagesc(labelledFrame)
%     axis image
    clusterContainer(:,:,frameInd)=uint16(labelledFrame);
end  

save('SegmentationWithSLIC_ICP','clusterContainer')


