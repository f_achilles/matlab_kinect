% Simulate moving object in depth sequence

W = 50; %px
H = 50; %px
numFrames = 30;

bgDistance = 2000; %mm
bgEdgeLen = 5000; %mm

objDistance = 1500; %mm
objShortEdge = 50; %mm
objLongEdge = 100; %mm

motionAmplitude = 200; %mm, sine motion from left to right to left again

% ray and plane intersection: http://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
% normals of wall plane and object plane
nWall = [0;0;1];
nObject = nWall;

% some point on each plane (just take central point)
p0Wall = [0;0;-bgDistance];
p0Object = [0;0;-objDistance];

% intersection points as abstract fcn objects
intersecPtWall = @(ray) (dot(p0Wall,nWall)/dot(ray,nWall))*ray;
intersecPtObject = @(ray) (dot(p0Object,nWall)/dot(ray,nObject))*ray;

% cast rays, check if inside object or not, store depth
pixelSize = 0.0093; %mm
focalLength = 5.453; %mm
depthContainer = zeros(H,W,numFrames,'uint16');
indFrame = 1;
for indWidth = 1:W
    for indHeight = 1:H
        rayVecNormalized = [(W/2-indWidth)*pixelSize;(H/2-indHeight)*pixelSize;-focalLength]/norm([(W/2-indWidth)*pixelSize;(H/2-indHeight)*pixelSize;-focalLength]);
        objPoint = intersecPtObject(rayVecNormalized);
        xyAbsObjPoint = abs(objPoint(1:2));
        if all(xyAbsObjPoint < [objShortEdge;objLongEdge]/2)
            depthContainer(indHeight, indWidth, indFrame) = -objPoint(3);
        else
            wallPoint = intersecPtWall(rayVecNormalized);
            depthContainer(indHeight, indWidth, indFrame) = uint-wallPoint(3);
        end
    end
end
figure; colormap(jet); imagesc(depthContainer(:,:,1))
axis image




