%% export range files as .png with grey colormap and have each frame duplicated

% run the import script with user input
sourceFolder = './data';
depthContainer = loadMat(sourceFolder);

% export as .png using imwrite
numFrames = size(depthContainer,3);
grayHarmonicMap = repmat(gray(750),10,1);
grayHarmonicVec = grayHarmonicMap(:,1);
mkdir([sourceFolder '/imagePairs']);
for frameInd = 1:numFrames
    imagePair = [depthContainer(:,:,frameInd) depthContainer(:,:,frameInd)];
    imagePair(imagePair==0)=1;
    indexedPair = reshape(grayHarmonicVec(imagePair(:)),size(imagePair));
    imshow(indexedPair);
    imwrite(indexedPair,[sourceFolder '/imagePairs/frame' num2str(frameInd) '.png']);
end

