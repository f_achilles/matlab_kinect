currPath=pwd;
% where the original recordings are
recDir='C:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings';
cd(recDir)
dirContent=dir;
depthSequence=[];
FramesTxt=fopen('.\MlabReadableFrameNumbers.txt','r');
christian=false;
% create folder, load depth stream, store labelled depth data
while ~feof(FramesTxt)
    cLine=fgetl(FramesTxt);
    actorInd=strfind(cLine,'#');
    actionInd=strfind(cLine,':');
    if ~isempty(actorInd)
        cd(recDir)
        cActor=cLine((actorInd(end)+1):end);
        if strcmp(cActor,'Christian')
        christian=true;
        else
            christian=false;
            continue
        end
        mkdir(cActor)
        for i=1:numel(dirContent)
            if ~isempty(regexpi(dirContent(i).name,[cActor '2cropped']))
                clear depthSequence
                load(dirContent(i).name)
            end
        end
        cd(cActor)
    elseif ~isempty(actionInd) && christian
        cAction=cLine(1:(actionInd-1));
        if strcmp(cAction,'LegLeft') 
            frameIndices=regexp(cLine,'[-|\s](\d+)','tokens');
            for i=3:round(numel(frameIndices)/2)
                cSequencePart=depthSequence(:,:,str2double(frameIndices{i*2-1}):str2double(frameIndices{i*2}));
                save([cActor '_' cAction '_' num2str(i)],'cSequencePart','-v7.3');
            end
        elseif ~isempty(strfind(cAction,'So4')) || strcmp(cAction,'LegRight')
            frameIndices=regexp(cLine,'[-|\s](\d+)','tokens');
            for i=1:round(numel(frameIndices)/2)
                cSequencePart=depthSequence(:,:,str2double(frameIndices{i*2-1}):str2double(frameIndices{i*2}));
                save([cActor '_' cAction '_' num2str(i)],'cSequencePart','-v7.3');
            end
        else
            %nada
        end
        clear('cSequencePart')
    else
        % nada
    end
end

cd(currPath)
