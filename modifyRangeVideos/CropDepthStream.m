clear all, close all
%% load depth streams and display first frame
seqPath = 'C:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings';
[depthSequence, filename]= loadDepthSequence(seqPath,'mat');

D = depthSequence(:,:,1);
figure;
% hD=imshow(D,[0 5000]);
hD=imagesc(D);
axis image

%% read corners and store into vars

% LL, UR

%% make new depth sequence and save to file
depthSequence = depthSequence(UR(1):LL(1),LL(2):UR(2),:);
% show result
D = depthSequence(:,:,1);
figure;
hD=imshow(D,[0 5000]);
drawnow
% save to file
save([seqPath filesep filename 'Cropped'], 'depthSequence', '-v7.3')
clear depthSequence