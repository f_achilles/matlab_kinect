clear all, close all
%% replay Kinect depth stream
% load recorded depth stream
matFiles = dir('./*.mat');
if numel(matFiles)~=0
    disp('Choose the recording to be replayed')
    for iFiles = 1:numel(matFiles)
        disp([num2str(iFiles) ': ' matFiles(iFiles).name])
    end
else
    error('no .mat files found in current folder') 
end
indFile = input('Type a valid number and press enter: ');
load(matFiles(indFile).name)

depthContainer = double(depthContainer);

% plot slected recording
numFrames = size(depthContainer,3);
D = depthContainer(:,:,1);
figure;
hD=imshow(D,[0 5000]);
ratio = floor(5000/256);
cubeMap = colormap('jet');
betterCubeMap = repmat(cubeMap,ratio,1);
colormap(betterCubeMap);

% choose smoothing algorithm
disp('[1]: Smoothen along time-dimension only')
disp('[2]: Smoothen in 3D')
smoothChoice = input('Choose smoothing algorithm and press enter: ');
switch smoothChoice
    case 1
        meanImage=mean(depthContainer,3);
        IMfilter
        depthContainer=IMfilter(depthContainer,timeDimKernel);
    case 2
    otherwise
        error('Invalid input for smoothing algorithm!')
end
for i=1:numFrames
    D=depthContainer(:,:,i);
    set(hD,'CDATA',D);
    drawnow;
    pause(0.03)
end

