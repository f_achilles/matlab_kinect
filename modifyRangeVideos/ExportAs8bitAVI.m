%% Make AVI video from 16bit data (depthContainer or clusterContainer)

% The fullBitDepthVideo is the depth video that should be stored.
% It needs to be a Matlab matrix and have the dimensions [HxWxN], where
% H = height (usually 480)
% W = width (usually 640)
% N = number of frames
fullBitDepthVideo = ChrisHalfBlanket100frames;

% One 16bit-frame is split into two parts (most significant and least
% significant bits, each 8bits in lengths), which are stacked along the
% rows-dimension (dimension number 1).
MSBPart = uint8(bitsra(fullBitDepthVideo, 8));
LSBPart = uint8(bitand(fullBitDepthVideo, 255));
videoContainer = cat(1,MSBPart,LSBPart);

% This produces an uncompressed AVI video
vidObj = VideoWriter('VideoNew.avi','Grayscale AVI');

open(vidObj);

for i=1:size(videoContainer,3)
    writeVideo(vidObj,struct('cdata',videoContainer(:,:,i),'colormap',[]));
end

close(vidObj);

clear videoContainer MSBPart LSBPart

% The file can susequently be compressed using 7zip-library or similar