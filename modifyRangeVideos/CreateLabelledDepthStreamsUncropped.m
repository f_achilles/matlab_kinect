currPath=pwd;
% where the original recordings are
recDir='D:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings\rotated';
cd(recDir)
dirContent=dir('*.mat');
depthSequence=[];
framesTxt=fopen('..\MlabReadableFNHalfBlanket.txt','r');
christian=true;
% create folder, load depth stream, store labelled depth data
while ~feof(framesTxt)
    cLine=fgetl(framesTxt);
    actorInd=strfind(cLine,'#');
    actionInd=strfind(cLine,':');
    if ~isempty(actorInd)
        cd(recDir)
        cActor=cLine((actorInd(end)+1):end);
        if ~isempty(strfind(cActor,'Christian'))
            christian=true;
        else
            christian=false;
            continue
        end
        mkdir(cActor)
        for i=1:numel(dirContent)
            if ~isempty(regexpi(dirContent(i).name,['Christian1BRotated']))
                clear depthSequence
                load(dirContent(i).name)
            end
        end
        cd(cActor)
    elseif ~isempty(actionInd) && christian
        cAction=cLine(1:(actionInd-1));
%         if strcmp(cAction,'LegLeft') 
%             frameIndices=regexp(cLine,'[-|\s](\d+)','tokens');
%             for i=3:round(numel(frameIndices)/2)
%                 cSequencePart=depthSequence(:,:,str2double(frameIndices{i*2-1}):str2double(frameIndices{i*2}));
%                 save([cActor '_' cAction '_' num2str(i)],'cSequencePart','-v7.3');
%             end
%         else
        if ~isempty(strfind(cAction,'So4')) || strcmp(cAction,'LegRight')
            frameIndices=regexp(cLine,'[-|\s](\d+)','tokens');
            for i=1:round(numel(frameIndices)/2)
                cSequencePart=depthSequence(:,:,str2double(frameIndices{i*2-1}):str2double(frameIndices{i*2}));
                save([cActor '_' cAction '_' num2str(i)],'cSequencePart','-v7.3');
            end
        else
            %nada
        end
        clear('cSequencePart')
    else
        % nada
    end
end

cd(currPath)
