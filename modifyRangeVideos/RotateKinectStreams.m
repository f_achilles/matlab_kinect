close all
%% load depth streams and display first frame
seqPath = 'C:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings';
[depthSequence, filename]= loadDepthSequence(seqPath,'mat');

D = depthSequence(:,:,1);

numFrames = size(depthSequence,3);

[monitorSize] = get(0,'MonitorPositions');
monitorWidth = monitorSize(3);
monitorHeight = monitorSize(4);

borderPixels = 20;
figure('position',[floor(monitorWidth/2)-640-borderPixels floor(monitorHeight/2)-240-borderPixels 2*borderPixels+2*640 2*borderPixels+480]);
subplot(1,2,1);
hD=imshow(D,[0 5000]);
if numel(dir([seqPath filesep filename '.avi']))~=0
    kinectMovie = VideoReader([seqPath filesep filename '.avi']);
else
    error('No RGB video found.')
end
I=read(kinectMovie,1);
subplot(1,2,2);
hI=imshow(I);
title('Before')

figure('position',[floor(monitorWidth/2)-640-borderPixels floor(monitorHeight/2)-240-borderPixels 2*borderPixels+2*640 2*borderPixels+480]);
subplot(1,2,1);
D=rot90(D,2);
hD=imshow(D,[0 5000]);
subplot(1,2,2);
I=permute(I,[2 1 3]);
I(:,:,1)=rot90(I(:,:,1),2);
I(:,:,2)=rot90(I(:,:,2),2);
I(:,:,3)=rot90(I(:,:,3),2);
hI=imshow(I);

title('After')

RGBVideo = VideoWriter('tmpRGBVideo.avi');
open(RGBVideo)
for i=1:numFrames
    I=permute(read(kinectMovie,i),[2 1 3]);
    I(:,:,1)=rot90(I(:,:,1),2);
    I(:,:,2)=rot90(I(:,:,2),2);
    I(:,:,3)=rot90(I(:,:,3),2);
%     set(hD,'CDATA',D);
%     set(hI,'CDATA',I);
%     drawnow;
    writeVideo(RGBVideo,struct('cdata',I,'colormap',[]));
end
close(RGBVideo)

parfor i=1:numFrames
    D=rot90(depthSequence(:,:,i),2);
    depthSequence(:,:,i)=D;
end

% Stop the Kinect Process
% mxNiDeleteContext(KinectHandles);

filename = [filename 'Rotated'];
save(filename, 'depthSequence','-v7.3')
movefile('tmpRGBVideo.avi', [filename '.avi'])
