%% export range files as .png with grey colormap and have each frame duplicated

% run the import script with user input
sourceFolder = './data';
depthContainer = loadMat(sourceFolder);

RGBsourceFolder = 'D:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings\rotated';
colorContainer = loadVidPart(RGBsourceFolder);

% export as .png using imwrite
numFrames = size(depthContainer,3);
grayHarmonicMap = repmat(gray(750),10,1);
grayHarmonicVec = grayHarmonicMap(:,1);
mkdir([sourceFolder '/imagePairs']);
for frameInd = 1:numFrames
    depthImage = depthContainer(:,:,frameInd);
    depthImage(depthImage==0)=1;
    indexedDepthImage = 255*reshape(grayHarmonicVec(depthImage(:)),size(depthImage));
    
    indexedDepthImage = cat(3, indexedDepthImage, indexedDepthImage, indexedDepthImage);
    
    colorImage = colorContainer(:,:,:,frameInd);
    
    imagePair = cat(2,indexedDepthImage,colorImage);
    
    imshow(imagePair);
    impixelinfo
    imwrite(imagePair,[sourceFolder '/imagePairs/frame' num2str(frameInd) '.png']);
end

