pathsaver = what;
currPath = pathsaver.path;
rawFilePath = 'F:\JoaoCunhaData\IM1170\Sz14\Event3';

% convert depth streams
cd([rawFilePath filesep 'Depth'])
imageFiles = dir('.\*.png');
numFiles = numel(imageFiles);

% import first frame
pngName = imageFiles(1).name;
depthFrame = rgb2gray(importdata(pngName));
depthFrame = rot90(depthFrame);
depthSequence = zeros([size(depthFrame) numFiles],'uint8');

depthVideo = VideoWriter('depthVideo.avi','Grayscale AVI');
open(depthVideo)
for i = 1:numFiles
    % load frame
    pngName = imageFiles(i).name;
    depthFrame = rgb2gray(importdata(pngName));
    % rotate frame
    depthFrame = rot90(depthFrame);
    % store frame
    writeVideo(depthVideo,depthFrame);
    depthSequence(:,:,i) = depthFrame;
end
close(depthVideo)
save('depthSequence.mat','depthSequence','-v7.3')

% convert RGB streams
cd([rawFilePath filesep 'Color'])
imageFiles = dir('.\*.png');
numFiles = numel(imageFiles);

colorVideo = VideoWriter('colorVideo.avi');
open(colorVideo)
for i = 1:numFiles
    % load frame
    pngName = imageFiles(i).name;
    colorFrame = importdata(pngName);
    % rotate frame
    colorFrameRot(:,:,1) = rot90(colorFrame(:,:,1));
    colorFrameRot(:,:,2) = rot90(colorFrame(:,:,2));
    colorFrameRot(:,:,3) = rot90(colorFrame(:,:,3));
    % store frame
    writeVideo(colorVideo,colorFrameRot);
end
close(colorVideo)

cd(currPath)