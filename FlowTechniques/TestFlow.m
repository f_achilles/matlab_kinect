load('depthEasyScene')

numFrames = size(depthContainer,3);
uvFlowContainer = zeros([size(depthContainer,1) size(depthContainer,2) 2*size(depthContainer,3)]);
for fInd = 1:(numFrames-1)
        uvFlowContainer(:,:,(2*(fInd-1)+1):(2*fInd)) = estimate_flow_interface(depthContainer(:,:,fInd), depthContainer(:,:,fInd+1), 'classic+nl-fast');
end


% the method "classic+nl-fast" completely fails on depth data that has
% coplanar surfaces

figure;
% Display estimated flow fields
for fInd = 1:(numFrames-1)
uv = uvFlowContainer(:,:,(2*(fInd-1)+1):(2*fInd));
subplot(1,2,1);imshow(uint8(flowToColor(uv))); title('Middlebury color coding');
subplot(1,2,2); plotflow(uv);   title('Vector plot');
pause
end