%% Clustering Segmentation
% So first: do NN rangesearch
% 

% Threshold for Clustering (in [mm])
thrClust = 150;

PointCloudFromDepth
close all
clear pc D validIndices
tic
kdtreePts = KDTreeSearcher([X Y Z]);
actIdx = 1;
families = X*0;
for familyCtr=1:numel(X);
    [actRegionIdx]=rangesearch(kdtreePts,[X(actIdx) Y(actIdx) Z(actIdx)],thrClust);
    % find crossection of tempFamilies and families
    % if there are some, assign new familyCounter to all crossections (find(all))
    if ~isempty(actRegionIdx)
    crossSect = families([actRegionIdx{1}]);
    crossSect = unique(crossSect(crossSect~=0));
    if ~isempty(crossSect)
        for i=1:numel(crossSect)
            families(families==crossSect(i))=familyCtr;
        end
    end
    families([actRegionIdx{1}])=familyCtr;
    end
    families(actIdx)=familyCtr;
    actIdx = find(families==0,1,'first');
    if isempty(actIdx)
        break;
    end
end
[uniqueFams,~,famIdx]=unique(families);
famVec=1:numel(uniqueFams);
families=famVec(famIdx);
toc
%% plot clustering
hist(families,numel(uniqueFams))
cm=jet;
figure
hold on
for i=famVec
plot3(X(families==i),Y(families==i),Z(families==i),'*','color',cm(mod(15*i,64)+1,:))
end
hold off
axis vis3d image
view(3)

%% impose some measures on the clusters to find the bed
clear famIdx
% calc diagonal of bounding box, bed should have one between 2 and 3 meters
diagLengths=famVec*0;
for i=famVec
    tempPts=[X(families==i),Y(families==i),Z(families==i)];
    tempMin=min(tempPts,[],1);
    tempMax=max(tempPts,[],1);
    diagLengths(i)=norm(tempMin-tempMax);
end
bedClusterCandidates=famVec(diagLengths<3000&diagLengths>2000);
% some more magic to determine which of the candidates is the bedCluster
%...
bedClusterIdx=bedClusterCandidates(1);
%...
idxBedRegion=find(families==bedClusterIdx);
%% center the selected bed cluster
% crop pointcloud to BedRegion, rotate into its own coordinate system
XBedRegion = X(idxBedRegion);
YBedRegion = Y(idxBedRegion);
ZBedRegion = Z(idxBedRegion);
BedRegionCenter = mean([XBedRegion YBedRegion ZBedRegion],1);
[princVectorsBedRegion] = pca([XBedRegion YBedRegion ZBedRegion]);

BedRegionLocalT = [princVectorsBedRegion BedRegionCenter'; 0 0 0 1];
ptsBedRegion = BedRegionLocalT\[XBedRegion YBedRegion ZBedRegion ones(numel(XBedRegion),1)]';
XBedRegion = ptsBedRegion(1,:)';
YBedRegion = ptsBedRegion(2,:)';
ZBedRegion = ptsBedRegion(3,:)';
% shift X and Y to median value
mX=median(XBedRegion);
mY=median(YBedRegion);
XBedRegion=XBedRegion-mX;
YBedRegion=YBedRegion-mY;
medianTransform = [eye(3) [-mX;-mY;0]; 0 0 0 1];
% fit plane to find Z up or down orientation...
[Ybed,Xbed]=meshgrid(-450:50:450,-1000:50:1000);
bedPts = [Xbed(:) Ybed(:) zeros(numel(Xbed),1)];
[RBed,TBed,icpErrors,times]=icp([XBedRegion YBedRegion ZBedRegion]',bedPts',70,'Matching','kDtree');
bedPts = bedPts*RBed'+repmat(TBed',size(bedPts,1),1);
%% plot bed-plane fitting result
figure
hold on
surf(reshape(bedPts(:,1),size(Xbed)),reshape(bedPts(:,2),size(Xbed)),reshape(bedPts(:,3),size(Xbed)),'facecolor','blue')
plot3(XBedRegion,YBedRegion,ZBedRegion,'r*')
hold off
axis vis3d image
%% find out where patient is, above or below the segmenting plane
HBed=[RBed TBed;0 0 0 1];
bedPtsInBedCoordsRightWay=[XBedRegion YBedRegion ZBedRegion ones(numel(XBedRegion),1)]*(inv(HBed))';
maxBorderBedPts=max([Xbed(:) Ybed(:) zeros(numel(Xbed),1)],[],1);
minBorderBedPts=min([Xbed(:) Ybed(:) zeros(numel(Xbed),1)],[],1);
indicesInPlane=all(bedPtsInBedCoordsRightWay(:,1:2)<repmat(maxBorderBedPts(1:2),numel(XBedRegion),1) & bedPtsInBedCoordsRightWay(:,1:2)>repmat(minBorderBedPts(1:2),numel(XBedRegion),1),2);
bedPtsInBedCoordsRightWay=bedPtsInBedCoordsRightWay(indicesInPlane,:);
% compute Kinect position inside the loacl bed frame
KinectPosition = inv(BedRegionLocalT*medianTransform);
KinectPosition = KinectPosition(1:3,4);
%above bed plane
indicesAbovePlane=bedPtsInBedCoordsRightWay(:,3)>=0;
ptCloudAbovePlane=bedPtsInBedCoordsRightWay(indicesAbovePlane,1:3);
% [StateVectorAbove, errAbove]=FitHumanToDepth(ptCloudAbovePlane);

clear X Y Z tempPts ptsBedRegion families
% create kDTree search object from pointcloud
kdtreePts = KDTreeSearcher(ptCloudAbovePlane);
% calculate the normals using the kdTree
normalsOfPts = calcNormalsWithPCA(kdtreePts,KinectPosition);

[StateVectorAbove, errAbove]=articulatedICP(kdtreePts,normalsOfPts,KinectPosition);
%below bed plane
indicesBelowPlane=bedPtsInBedCoordsRightWay(:,3)<0;
ptCloudBelowPlane=bedPtsInBedCoordsRightWay(indicesBelowPlane,1:3);
[StateVectorBelow, errBelow]=FitHumanToDepth(ptCloudBelowPlane);

%% Presentation Pipeline
clear all
close all
PointCloudFromDepth
%
heaviestPoint = mean([X Y Z],1);
[xsp,ysp,zsp]=sphere(20);
xsp=50*xsp+heaviestPoint(1);
ysp=50*ysp+heaviestPoint(2);
zsp=50*zsp+heaviestPoint(3);
hold on
surf(xsp,ysp,zsp,'facecolor','red','edgecolor','none')
hold off
%
[princVectors] = pca([X Y Z]);
hold on
plot3([heaviestPoint(1) heaviestPoint(1)+1000*princVectors(1,1)],[heaviestPoint(2) heaviestPoint(2)+1000*princVectors(2,1)],[heaviestPoint(3) heaviestPoint(3)+1000*princVectors(3,1)],'r','linewidth',5)
plot3([heaviestPoint(1) heaviestPoint(1)+1000*princVectors(1,2)],[heaviestPoint(2) heaviestPoint(2)+1000*princVectors(2,2)],[heaviestPoint(3) heaviestPoint(3)+1000*princVectors(3,2)],'g','linewidth',5)
plot3([heaviestPoint(1) heaviestPoint(1)+1000*princVectors(1,3)],[heaviestPoint(2) heaviestPoint(2)+1000*princVectors(2,3)],[heaviestPoint(3) heaviestPoint(3)+1000*princVectors(3,3)],'b','linewidth',5)
hold off
%%
% make plane grid
[Ybed,Xbed]=meshgrid(-450:50:450,-1000:50:1000);
bedPts = [Xbed(:) Ybed(:) zeros(numel(Xbed),1)];
bedPts = bedPts*princVectors';
bedPts = bedPts + repmat(heaviestPoint,size(bedPts,1),1);
hold on
surf(reshape(bedPts(:,1),size(Xbed)),reshape(bedPts(:,2),size(Xbed)),reshape(bedPts(:,3),size(Xbed)),'facecolor','blue')
hold off
%%
% match plane grid to pointcloud
[R,T,icpErrors,times]=icp([X Y Z]',bedPts',70,'Matching','kDtree')
bedPts = bedPts*R'+repmat(T',size(bedPts,1),1);

% use plane grid matched position to segment bed & patient (=BedRegion)
kdtreePts = KDTreeSearcher([X Y Z]);
[idxBedRegion]=rangesearch(kdtreePts,bedPts,200);
idxBedRegion=unique([idxBedRegion{:}]);
% plot BedRegion
hold on
plot3(X(idxBedRegion),Y(idxBedRegion),Z(idxBedRegion),'r*')
hold off
%%
% crop pointcloud to BedRegion, rotate into its own coordinate system
XBedRegion = X(idxBedRegion);
YBedRegion = Y(idxBedRegion);
ZBedRegion = Z(idxBedRegion);
BedRegionCenter = mean([XBedRegion YBedRegion ZBedRegion],1);
[princVectorsBedRegion] = pca([XBedRegion YBedRegion ZBedRegion]);

BedRegionLocalT = [princVectorsBedRegion BedRegionCenter'; 0 0 0 1];
ptsBedRegion = BedRegionLocalT\[XBedRegion YBedRegion ZBedRegion ones(numel(XBedRegion),1)]';
XBedRegion = ptsBedRegion(1,:)';
YBedRegion = ptsBedRegion(2,:)';
ZBedRegion = ptsBedRegion(3,:)';

% make new triangulation
Tri = delaunay(XBedRegion,YBedRegion);
% and plot
trisurf(Tri,XBedRegion,YBedRegion,ZBedRegion,'facecolor','interp','edgecolor','none')
axis vis3d image
lighting gouraud
camlight
% looking good so far!

%% Pipeline without Plots
% now match the FULL human model to the segmented region
PointCloudFromDepth
clear pc validIndices
close all
heaviestPoint = mean([X Y Z],1);
[princVectors] = pca([X Y Z]);
% make plane grid
[Ybed,Xbed]=meshgrid(-450:50:450,-1000:50:1000);
bedPts = [Xbed(:) Ybed(:) zeros(numel(Xbed),1)];
bedPts = bedPts*princVectors';
bedPts = bedPts + repmat(heaviestPoint,size(bedPts,1),1);
% match plane grid to pointcloud
[R,T,icpErrors,times]=icp([X Y Z]',bedPts',70,'Matching','kDtree')
bedPts = bedPts*R'+repmat(T',size(bedPts,1),1);
% use plane grid matched position to segment bed & patient (=BedRegion)
kdtreePts = KDTreeSearcher([X Y Z]);
[idxBedRegion]=rangesearch(kdtreePts,bedPts,200);
idxBedRegion=unique([idxBedRegion{:}]);
% crop pointcloud to BedRegion, rotate into its own coordinate system
XBedRegion = X(idxBedRegion);
YBedRegion = Y(idxBedRegion);
ZBedRegion = Z(idxBedRegion);
BedRegionCenter = mean([XBedRegion YBedRegion ZBedRegion],1);
[princVectorsBedRegion] = pca([XBedRegion YBedRegion ZBedRegion]);

BedRegionLocalT = [princVectorsBedRegion BedRegionCenter'; 0 0 0 1];
ptsBedRegion = BedRegionLocalT\[XBedRegion YBedRegion ZBedRegion ones(numel(XBedRegion),1)]';
XBedRegion = ptsBedRegion(1,:)';
YBedRegion = ptsBedRegion(2,:)';
ZBedRegion = ptsBedRegion(3,:)';

[x, err]=FitHumanToDepth([XBedRegion YBedRegion ZBedRegion]);

%% plot Human model with points that are visible form KinectViewPoint
KinectViewPoint = [0;-600;1400];
PlotHumanModel
visibleIndices = calcSelfOcclusion(HumanModelVert, HumanModelNormals, KinectViewPoint);
hold on
plot3(HumanModelVert(1,visibleIndices),HumanModelVert(2,visibleIndices),HumanModelVert(3,visibleIndices),'r*')
plot3(KinectViewPoint(1),KinectViewPoint(2),KinectViewPoint(3),'g*','MarkerSize',20)
hold off
axis tight
figure
hold on
plot3(HumanModelVert(1,visibleIndices),HumanModelVert(2,visibleIndices),HumanModelVert(3,visibleIndices),'r*')
plot3(KinectViewPoint(1),KinectViewPoint(2),KinectViewPoint(3),'g*','MarkerSize',20)
hold off
axis vis3d image

%% plot of cluster result
load('clusterContainer10sWavingLHandFelix1.mat')
maxInd = double(max(clusterContainer(:)));
clusIMG = imagesc(clusterContainer(:,:,1));
colormap(colorcube(double(max(max(clusterContainer(:,:,1))))));
colMP = colormap(colorcube(double(max(max(clusterContainer(:,:,1))))));
caxis auto
axis image
set(gca,'xdir','reverse')
clusteringVideo = VideoWriter('PrettierPatches.avi');
open(clusteringVideo)
for i=1:100
    set(clusIMG,'cdata',clusterContainer(:,:,i));
    drawnow;
%     pause(0.1);
    % store frame
    writeVideo(clusteringVideo,struct('cdata',double(clusterContainer(:,:,i)),'colormap',colMP));
end
close(clusteringVideo)
