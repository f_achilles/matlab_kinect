function visibleIndices = calcSelfOcclusion(vertices, normals, viewpoint, triangulation)
% first step is a simple rejection of points whose surface points away from
% the camera viewpoint
raysToCamera = repmat(viewpoint,1,size(vertices,2))-vertices;
plusOneMeansVisible=sign(dot(raysToCamera,normals));
visibleIndices = find(plusOneMeansVisible==1);

% those have to be further reduced, analyzing the triangulation and casting
% rays for each remaining vertex, checking if it intersects any triangles
% on its way to the viewpoint
%...
end