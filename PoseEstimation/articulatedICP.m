% Load depth frame and fit human model over articulated ICP approach.
function [xState,errorVal]= articulatedICP(kdtreePts,normalsOfPts,KinectPosition)
if ~exist('kdtreePts','var')
PointCloudFromDepth
clear pc validIndices
close all
KinectPosition = [0;0;0];
% create kDTree search object from pointcloud
kdtreePts = KDTreeSearcher([X Y Z]);
% calculate the normals using the kdTree
normalsOfPts = calcNormalsWithPCA(kdtreePts,KinectPosition);
end
if ~exist('KinectPosition','var')
    error('Specify position of the Kinect camera in a 3x1 vector')
end

% still missing> KinectPosition
objFcn = @(x)objectiveFcnRMSWithNormals(kdtreePts,x,normalsOfPts,KinectPosition);
initParams = zeros(32,1);
% rotate the inital model so that the head points in +x direction
initParams(4)=pi/2;
initParams(5)=-pi/2;

[HumanModelVert, HumanModelNormals]=updateHumanModel(initParams);
numHumanModelPts = size(HumanModelVert,2);
visibleVerticesIndex = calcSelfOcclusion(HumanModelVert,HumanModelNormals,KinectPosition);

figure
hold on
plot3(HumanModelVert(1,visibleVerticesIndex),HumanModelVert(2,visibleVerticesIndex),HumanModelVert(3,visibleVerticesIndex),'r*')
plot3(KinectPosition(1),KinectPosition(2),KinectPosition(3),'g*','MarkerSize',20)
hold off
axis vis3d image
title('Human model in initial position with culled visibility due to Kinect Viewpoint')

% perform 20 ICP iterations to get a good initial pose
q=(kdtreePts.X)';
[R, T] = icp(q,HumanModelVert(:,visibleVerticesIndex),20,'Matching','kDtree');
HumanModelVert = R*HumanModelVert+repmat(T,1,size(HumanModelVert,2));
Rinit = angle2dcm(initParams(4),initParams(5),initParams(6));
[y, p, r]=dcm2angle(R*Rinit);
initParams(1:6) = [R*initParams(1:3)+T/1000; y;p;r];

figure
hold on
plot3(HumanModelVert(1,:),HumanModelVert(2,:),HumanModelVert(3,:),'*')
plot3(q(1,:),q(2,:),q(3,:),'r.')
hold off
axis vis3d image
view(3)
drawnow
title('Human model in position after visible-points-ICP')
% pause

% parameters for human model:
% 1: Xcenter
% 2: Ycenter
% 3: Zcenter
% 4: Yawcenter
% 5: Pitchcenter
% 6: Rollcenter
% 7: YawUpperArmL
% 8: PitchUpperArmL
% 9: RollUpperArmL
%10: PhiLowerArmL
%11: YawHandL
%12: PitchHandL
%13: RollHandL
%14: YawUpperArmR
%15: PitchUpperArmR
%16: RollUpperArmR
%17: PhiLowerArmR
%18: YawHandR
%19: PitchHandR
%20: RollHandR
%21: YawUpperLegL
%22: PitchUpperLegL
%23: RollUpperLegL
%24: PhiLowerLegL
%25: YawFootL
%26: RollFootL
%27: YawUpperLegR
%28: PitchUpperLegR
%29: RollUpperLegR
%30: PhiLowerLegR
%31: YawFootR
%32: RollFootR

upperBounds = [1;1;1;pi;pi/2;pi];
lowerBounds = [-1;-1;-1;-pi;-pi/2;-pi];
% docu for bounds
% ARMS
% BoundsUpperArmL (7:9)
% Around -z the Maximum rotation is about 3*pi/5 (e.g. left arm bent behind back)
% Around -y the Maximum rotation is about pi (e.g. left arm bent upwards over the side)
% Around -x the Maximum rotation is about pi (e.g. left arm bent upwards over the front)
upperBounds(7:9)=[3*pi/5;pi;pi];
% Around -z the minimum rotation is about -pi/2 (left arm bent fully outwards)
% Around -y the minimum rotation is 0 (stretched down)
% Around -x the minimum rotation is about -pi/2 (e.g. left arm stretched behind the back)
lowerBounds(7:9)=[-pi/2;0;-pi/2];
% BoundsElbowL (10)
% Around -x the Maximum rotation is about 4*pi/5 (fully bent elbow)
upperBounds(10)=4*pi/5;
% Around -x the minumum rotation is 0 (fully stretched elbow)
lowerBounds(10)=0;
% BoundsHandL (11:13) !! Hands are initialized with palms facing front !!
% (just random definition)
% Around -z the Maximum rotation is about pi (hand palm inwards rotated, facing back)
% Around -y the Maximum rotation is about pi/4 (left hand bent to the left)
% Around -x the Maximum rotation is about pi/2 (hand palm facing upwards)
upperBounds(11:13)=[pi;pi/4;pi/2];
% Around -z the minimum rotation is about -pi/5 (hand palm rotated fully outwards)
% Around -y the minimum rotation is -3*pi/4 (left hand bent to the right)
% Around -x the minimum rotation is about -pi/2 (hand palm facing down)
lowerBounds(11:13)=[-pi/5;-3*pi/4;-pi/2];
% BoundsUpperArmR (14:16)
% Around -z the Maximum rotation is about pi/2 (right arm bent fully
% outwards)#
% Around -y the Maximum rotation is about 0 (stretched down)#
% Around -x the Maximum rotation is about pi (right arm bent upwards over
% the front)#
upperBounds(14:16)=[pi/2;0;pi];
% Around -z the minimum rotation is about -3*pi/5 (right arm bent behind
% back)#
% Around -y the minimum rotation is -pi (right arm bent upwards over the
% side)#
% Around -x the minimum rotation is about -pi/2 (e.g. right arm stretched
% behind the back)#
lowerBounds(14:16)=[-3*pi/5;-pi;-pi/2];
% BoundsElbowR (17)
% Around -x the Maximum rotation is about 4*pi/5 (fully bent elbow)#
upperBounds(17)=4*pi/5;
% Around -x the minumum rotation is 0 (fully stretched elbow)#
lowerBounds(17)=0;
% BoundsHandR (18:20) !! Hands are initialized with palms facing front !!
% (just random definition)
% Around -z the Maximum rotation is about pi/5 (hand palm rotated fully
% outwards)#
% Around -y the Maximum rotation is about 3*pi/4 (right hand bent to the left)
% Around -x the Maximum rotation is about pi/2 (hand palm facing upwards)#
upperBounds(18:20)=[pi/5;3*pi/4;pi/2];
% Around -z the minimum rotation is about -pi (hand palm inwards rotated,
% facing back)#
% Around -y the minimum rotation is -pi/4 (right hand bent to the right)
% Around -x the minimum rotation is about -pi/2 (hand palm facing down)#
lowerBounds(18:20)=[-pi;-pi/4;-pi/2];
% LEGS
% BoundsUpperLegL (21:23)
% Around -z the Maximum rotation is about pi/4 (left leg rotated inwards)
% Around -y the Maximum rotation is about pi/4 (left leg bent to the left side)
% Around -x the Maximum rotation is about 4*pi/5 (left leg bent upwards to chest)
upperBounds(21:23)=[pi/4;pi/4;4*pi/5];
% Around -z the minimum rotation is about -pi/4 (left leg rotated outwards)
% Around -y the minimum rotation is -pi/4 (left leg crossed to the right side)
% Around -x the minimum rotation is about -pi/5 (left leg stretched backwards)
lowerBounds(21:23)=[-pi/4;-pi/4;-pi/5];
% BoundsKneeL (24)
% Around -x the Maximum rotation is about 0 (fully stretched left knee)
upperBounds(24)=0;
% Around -x the minumum rotation is -4*pi/5 (fully bent left knee)
lowerBounds(24)=-4*pi/5;
% BoundsFootL (25:26)
% Around -z the Maximum rotation is about pi/5 (left foot rotated inwards)
% Around -x the Maximum rotation is about pi/6 (left foot pulled up)
upperBounds(25:26)=[pi/5;pi/6];
% Around -z the minimum rotation is about -pi/5 (left foot rotated outwards)
% Around -x the minimum rotation is about -3*pi/4 (left foot pointing down)
lowerBounds(25:26)=[-pi/5;-3*pi/4];
% BoundsUpperLegR (27:29)
% Around -z the Maximum rotation is about pi/4 (right leg rotated outwards)
% Around -y the Maximum rotation is about pi/4 (right leg crossed to the left side)
% Around -x the Maximum rotation is about 4*pi/5 (right leg bent upwards to chest)
upperBounds(27:29)=[pi/4;pi/4;4*pi/5];
% Around -z the minimum rotation is about -pi/4 (right leg rotated inwards)
% Around -y the minimum rotation is -pi/4 (right leg bent to the right side)
% Around -x the minimum rotation is about -pi/5 (right leg stretched backwards)
lowerBounds(27:29)=[-3*pi/5;-pi;-pi/2];
% BoundsKneeR (30)
% Around -x the Maximum rotation is 0 (fully stretched right knee)
upperBounds(30)=0;
% Around -x the minumum rotation is -4*pi/5 (fully bent right knee)
lowerBounds(30)=-4*pi/5;
% BoundsFootR (31:32)
% Around -z the Maximum rotation is about pi/5 (right foot rotated outwards)
% Around -x the Maximum rotation is about pi/6 (right foot pulled up)
upperBounds(31:32)=[pi/5;pi/6];
% Around -z the minimum rotation is about -pi/5 (right foot rotated inwards)
% Around -x the minimum rotation is about -3*pi/4 (right foot pointing down)
lowerBounds(31:32)=[-pi/5;-3*pi/4];
if ~all(lowerBounds<upperBounds)
    error('Joint angle boundaries wrongly defined!');
end

% Simulated Annealing
% OPTIONS = saoptimset(@simulannealbnd);
% OPTIONS.TimeLimit = 30;
% OPTIONS.TolFun = 50;
% simulated annealing
% [x,error,exitFlag,output] = simulannealbnd(objFcn,initParams,lowerBounds,upperBounds,OPTIONS);

% Nelder/Mead simplex search
% OPTIONS = optimset('fminsearch');
% OPTIONS.Display = 'iter';
% OPTIONS.MaxFunEvals = 100;
% tic
% [xStateBody,errorRaw] = fminsearchbnd(objFcn,initParams,lowerBounds,upperBounds,OPTIONS);
% toc

% Levenberg-Marquardt approach
options = optimoptions('lsqnonlin');
options.MaxIter = 25;
options.Display = 'iter';
% [x,error,exitFlag,output] = lsqnonlin(objFcn, initParams, lowerBounds, upperBounds, options);

% hierarchical body part based loop
bigLoopIterLimit = 10;
bigLoopCounter = 0;
xState = initParams;
errorVal=100;
oldErrorVal=150;
steadyStateThreshold = 5;
while(errorVal>50 && bigLoopCounter<bigLoopIterLimit && (oldErrorVal-errorVal)>steadyStateThreshold)
    initParams = xState;
    oldErrorVal = errorVal;
    for bodyPart={'abdomen','legUpperLeft','legUpperRight','armUpperLeft','armUpperRight','armLowerLeft','handLeft','armLowerRight','handRight','legLowerLeft','footLeft','legLowerRight','footRight'}
    % fit model by moving one body part at a time
    [bodyPartIndex,negatedIndexPt1,negatedIndexPt2]=getBodyPartIndex(bodyPart);
%     objFcn = @(x)objectiveFcnRMS(kdtreePts,[initParams(negatedIndexPt1); x; initParams(negatedIndexPt2)]);
    objFcn = @(x)objectiveFcnRMSWithNormals(kdtreePts,[initParams(negatedIndexPt1); x; initParams(negatedIndexPt2)],normalsOfPts,KinectPosition);
    [xStateBody,errorRaw] = lsqnonlin(objFcn, initParams(bodyPartIndex), lowerBounds(bodyPartIndex), upperBounds(bodyPartIndex), options);
%     [xStateBody,errorRaw] = fminsearchbnd(objFcn,initParams(bodyPartIndex),lowerBounds(bodyPartIndex),upperBounds(bodyPartIndex),OPTIONS);
    initParams(bodyPartIndex)=xStateBody;
    end
    xState=initParams;
    errorVal=sqrt(2*errorRaw/numHumanModelPts);
%     errorVal=errorRaw;
    bigLoopCounter = bigLoopCounter+1;
end
disp(xState)
disp('Error:')
disp(errorVal)
HumanModelVert=updateHumanModel(xState);
hold on
plot3(HumanModelVert(1,:),HumanModelVert(2,:),HumanModelVert(3,:),'k*')
hold off
title('Target Pointcloud in red, initial ICP fit in blue, articulated hierarchical ICP fit in black.')
end

function error = objectiveFcnRMS(kdtreePts,params)
    HumanModelVert=updateHumanModel(params);
    % perform k nearest neighbor search to get distance error
    [~,dists]=knnsearch(kdtreePts,HumanModelVert');
    RMSclosestPts = rms(dists);
    MAXclosestPts = max(dists);
    
    % a nice error measure for Ptcloud to Surface Scan should be more
    % intelligent than maximum/RMS distance
    %
    % Optimally, one half of the 3D model fits perfectly to the data and
    % the other half has their nearest neigbors at the maximum distance.
    % This maximum distance is hard to define, so one could update it to
    % always be the worst nearest neighbor.
    % What should be minimized then is the difference of the 50% best
    % matches to "0" plus the difference of the 50% worst matches to "max".
    
    % PtCloudSurfaceMeasure (RMS version)
    medianDist=median(dists);
    alpha=5;
    beta=1;
    RMSClosestFurthest = alpha*rms(dists(dists<=medianDist))+beta*rms(dists(dists>medianDist)-MAXclosestPts)/(alpha+beta);
    RMSClosest50percent = rms(dists(dists<=medianDist));
    PureDistancesClosest50percent = dists(dists<=medianDist);
    error = PureDistancesClosest50percent;
end

function [bodyPartIndex,negatedIndexPt1,negatedIndexPt2]=getBodyPartIndex(bodyPart)
    numParams = 32;
    switch bodyPart{1}
        case 'abdomen'
            bodyPartIndex = 1:6;
        case 'armUpperLeft'
            bodyPartIndex = 7:9;
        case 'armLowerLeft'
            bodyPartIndex = 10;
        case 'handLeft'
            bodyPartIndex = 11:13;
        case 'armUpperRight'
            bodyPartIndex = 14:16;
        case 'armLowerRight'
            bodyPartIndex = 17;
        case 'handRight'
            bodyPartIndex = 18:20;
        case 'legUpperLeft'
            bodyPartIndex = 21:23;
        case 'legLowerLeft'
            bodyPartIndex = 24;
        case 'footLeft'
            bodyPartIndex = 25:26;
        case 'legUpperRight'
            bodyPartIndex = 27:29;
        case 'legLowerRight'
            bodyPartIndex = 30;
        case 'footRight'
            bodyPartIndex = 31:32;
        otherwise
            error('bodyPart identifier not known.')
    end
    if bodyPartIndex(1)==1
        negatedIndexPt1=[];
    else
        negatedIndexPt1=1:(bodyPartIndex(1)-1);
    end
    if bodyPartIndex(end)==numParams
        negatedIndexPt2=[];
    else
        negatedIndexPt2=(bodyPartIndex(end)+1):numParams;
    end
end