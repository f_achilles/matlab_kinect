function error = objectiveFcnRMSWithNormals(kdtreePts,bodyModelParams,normalsOfPts,KinectPosition)
    
    % update the human body model
    [HumanModelVertices, HumanModelNormals]=updateHumanModel(bodyModelParams);
    
%     visibleVerticesIndex=1:size(HumanModelVertices,2);
    % crop out points that are not visible (self occlusion)
    visibleVerticesIndex = calcSelfOcclusion(HumanModelVertices,HumanModelNormals,KinectPosition);
    
    % perform k nearest neighbor search to get distance error
    [NNindices,dists]=knnsearch(kdtreePts,HumanModelVertices(:,visibleVerticesIndex)');
    
    % find double assignments of pointcloud points to model points
    % reduce the assignments so that only the closest model point wins
    [~,~,C]=unique(NNindices);
    occurrenceMatrix=repmat((1:numel(visibleVerticesIndex)),numel(visibleVerticesIndex),1)==repmat(C,1,numel(visibleVerticesIndex));
    multipleOccurrences=find(sum(occurrenceMatrix,1)>1);
    for i=multipleOccurrences
        distsOfConflictingModelpoints = dists(occurrenceMatrix(:,i));
        dists(occurrenceMatrix(:,i)) = min(distsOfConflictingModelpoints);
    end
    
    % Reverse-scale distances with the dot product of the normals of
    % corresponding points
    normalMatchMultiplier =(1-dot(normalsOfPts(NNindices,:),HumanModelNormals(:,visibleVerticesIndex)',2));
    % random penalty if matches are made to points that do not have a
    % defined normal
    normalMatchMultiplier(isnan(normalMatchMultiplier))=1;
    distsWithNormals = dists+dists.*normalMatchMultiplier;
    
    % a nice error measure for Ptcloud to Surface Scan should be more
    % intelligent than maximum/RMS distance
    %
    % Optimally, one half of the 3D model fits perfectly to the data and
    % the other half has their nearest neigbors at the maximum distance.
    % This maximum distance is hard to define, so one could update it to
    % always be the worst nearest neighbor.
    % What should be minimized then is the difference of the 50% best
    % matches to "0" plus the difference of the 50% worst matches to "max".
    
    dists = distsWithNormals;
%     RMSclosestPts = rms(dists);
%     MAXclosestPts = max(dists);
    
    % PtCloudSurfaceMeasure (RMS version)
%     medianDist=median(dists);
%     alpha=5;
%     beta=1;
%     RMSClosestFurthest = alpha*rms(dists(dists<=medianDist))+beta*rms(dists(dists>medianDist)-MAXclosestPts)/(alpha+beta);
%     RMSClosest50percent = rms(dists(dists<=medianDist));
%     PureDistancesClosest50percent = dists(dists<=medianDist);

% return full distance vector for lsqnonlin solver
errorvec=ones(size(HumanModelVertices,2),1)*mean(dists);
errorvec(visibleVerticesIndex) = dists;

error = errorvec;
    
end