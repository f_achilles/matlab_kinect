function [HumanModelVert, HumanModelNormals] = updateHumanModel(params)
% TODO: apply makeCylBodyPart() to abdomen and neck
% TODO: reduce number of vertices, use different Cylinders for different
% radii


% define static variables
persistent uniqueCylinderPts uniqueCubiclePts uniqueCubiclePtsHands uniqueSpherePts uniqueCylinderNormals uniqueCubicleNormals uniqueCubicleNormalsHands uniqueSphereNormals 
persistent headRadius neckLength neckRadius abdomenLength abdomenRadius1 abdomenRadius2 armUpperLength armUpperRadius armLowerLength armLowerRadius handLength handWidth handDepth legUpperLength legUpperRadius legLowerLength legLowerRadius footLength footWidth footDepth pointsPerCircle
if isempty(headRadius)
% body properties
% lengths in [mm]
headRadius = 100; %sphere

neckLength = 50; %cylinder
neckRadius = 70;

abdomenLength = 600; %ellipticCylinder
abdomenRadius1 = 175;
abdomenRadius2 = 75;

armUpperLength = 300; %cylinders
armUpperRadius = 50;
armLowerLength = 300;
armLowerRadius = 40;

handLength = 150; %cubicle
handWidth = 90;
handDepth = 30;

legUpperLength = 450; %cylinders
legUpperRadius = 75;
legLowerLength = 400;
legLowerRadius = 50;

footLength = 200; %cubicle
footWidth = 90;
footDepth = 50;

% rendering properties
pointsPerCircle = 20; % for cylinders and spheres
end

% creation of body parts
% preparations
if isempty(uniqueCylinderPts)
    % make unique cylindric points(abdomen, arms, legs)
    [xabd,yabd,zabd]=cylinder(ones(pointsPerCircle,1),pointsPerCircle); %standard cylinder is centered at x=y=0, z=0.5
    zabd=zabd-0.5;
    % erase double vertices
    uniqueCylinderPts=unique([xabd(:),yabd(:),zabd(:)],'rows');
    uniqueCylinderNormals = [uniqueCylinderPts(:,1) uniqueCylinderPts(:,2) uniqueCylinderPts(:,3)*0]';
end
if isempty(uniqueCubiclePts)
    % make unique cubicle points (hands, feet)
    [yha,xha]=meshgrid(linspace(-0.5,0.5,lower(pointsPerCircle/2)),linspace(-0.5,0.5,lower(pointsPerCircle/2)));
    zha=[0.5*ones(size(xha)) -0.5*ones(size(xha))];
    xha=[xha xha];
    yha=[yha yha];
    uniqueCubiclePts = [xha(:) yha(:) zha(:)];
    uniqueCubicleNormals = [0*xha(:) 0*yha(:) 2*zha(:)]';
    clear xha zha yha
    XRot90deg = angle2dcm(0,0,pi/2);
    uniqueCubiclePtsHands = uniqueCubiclePts*XRot90deg';
    uniqueCubicleNormalsHands = XRot90deg*uniqueCubicleNormals;
end
if isempty(uniqueSpherePts)
    [xh,yh,zh]=sphere(pointsPerCircle);
    % erase double vertices
    uniqueSpherePts=unique([xh(:),yh(:),zh(:)],'rows');
    uniqueSphereNormals = uniqueSpherePts';
end

% static transformation matrices
HFootCtrToAnkle = [eye(3) [0;-footLength/2;0]; 0 0 0 1];
HArmCtrToShoulder = [eye(3) [0;0;-armUpperLength/2]; 0 0 0 1];
HLowerArmCtrToElbow = [eye(3) [0;0;-armLowerLength/2]; 0 0 0 1];
HHandCtrToWrist = [eye(3) [0;0;-handLength/2]; 0 0 0 1];
HLegCtrToHip = [eye(3) [0;0;-legUpperLength/2]; 0 0 0 1];
HLowerLegCtrToElbow = [eye(3) [0;0;-legLowerLength/2]; 0 0 0 1];

% make abdomen
abdomenCenter = 1000*params(1:3);
% scale
xabd=uniqueCylinderPts(:,1)*abdomenRadius1;%+abdomenCenter(1);
yabd=uniqueCylinderPts(:,2)*abdomenRadius2;
zabd=uniqueCylinderPts(:,3)*abdomenLength;
% rotation
RMatAbdomen = angle2dcm(params(4),params(5),params(6));
ptsAbdomen = RMatAbdomen*[xabd yabd zabd]';
% translation
ptsAbdomen = ptsAbdomen+repmat(abdomenCenter,1,numel(xabd)); %3xNAbd
normalsAbdomen = RMatAbdomen*uniqueCylinderNormals;

% make head
headCenter = abdomenCenter + (headRadius+abdomenLength/2)*RMatAbdomen(:,3);
% scale
ptsHead=uniqueSpherePts'*headRadius;
% rotation
ptsHead=RMatAbdomen*ptsHead;
% translation
ptsHead=ptsHead+repmat(headCenter,1,size(ptsHead,2));
normalsHead = RMatAbdomen*uniqueSphereNormals;

% make neck
neckCenter = abdomenCenter + (neckLength/2+abdomenLength/2)*RMatAbdomen(:,3);
% scale
xn=uniqueCylinderPts(:,1)*neckRadius;
yn=uniqueCylinderPts(:,2)*neckRadius;
zn=uniqueCylinderPts(:,3)*neckLength;
% rotation
ptsNeck = RMatAbdomen*[xn yn zn]';
% translation
ptsNeck=ptsNeck+repmat(neckCenter,1,size(ptsNeck,2));
normalsNeck = RMatAbdomen*uniqueCylinderNormals;

% make arms
% left upper arm
HAbd = [RMatAbdomen abdomenCenter; 0 0 0 1];
RMatUpperArmL = angle2dcm(params(7),params(8),params(9));
HShoulderLToAbd = [RMatUpperArmL [abdomenRadius1+armUpperRadius;0;abdomenLength/2]; 0 0 0 1];
HUpperArmLToBase = HAbd*HShoulderLToAbd*HArmCtrToShoulder;
RUpperArmL = HUpperArmLToBase(1:3,1:3);
CenterUpperArmL = HUpperArmLToBase(1:3,4);
ptsUpperArmL = makeCylBodyPart(armUpperRadius,armUpperRadius,armUpperLength,CenterUpperArmL,RUpperArmL,uniqueCylinderPts);
normalsUpperArmL = RUpperArmL*uniqueCylinderNormals;

% right upper arm
RMatUpperArmR = angle2dcm(params(14),params(15),params(16));
HShoulderRToAbd = [RMatUpperArmR [-abdomenRadius1-armUpperRadius;0;abdomenLength/2]; 0 0 0 1];
HUpperArmRToBase = HAbd*HShoulderRToAbd*HArmCtrToShoulder;
RUpperArmR = HUpperArmRToBase(1:3,1:3);
CenterUpperArmR = HUpperArmRToBase(1:3,4);
ptsUpperArmR = makeCylBodyPart(armUpperRadius,armUpperRadius,armUpperLength,CenterUpperArmR,RUpperArmR,uniqueCylinderPts);
normalsUpperArmR = RUpperArmR*uniqueCylinderNormals;

% left lower arm
RMatLowerArmL = angle2dcm(0,0,params(10));
HElbowLToUpperArmL = [RMatLowerArmL [0;0;-armUpperLength/2]; 0 0 0 1];
HLowerArmLtoBase = HUpperArmLToBase*HElbowLToUpperArmL*HLowerArmCtrToElbow;
RLowerArmL = HLowerArmLtoBase(1:3,1:3);
CenterLowerArmL = HLowerArmLtoBase(1:3,4);
ptsLowerArmL = makeCylBodyPart(armLowerRadius,armLowerRadius,armLowerLength,CenterLowerArmL,RLowerArmL,uniqueCylinderPts);
normalsLowerArmL = RLowerArmL*uniqueCylinderNormals;

% right lower arm
RMatLowerArmR = angle2dcm(0,0,params(17));
HElbowRToUpperArmR = [RMatLowerArmR [0;0;-armUpperLength/2]; 0 0 0 1];
HLowerArmRtoBase = HUpperArmRToBase*HElbowRToUpperArmR*HLowerArmCtrToElbow;
RLowerArmR = HLowerArmRtoBase(1:3,1:3);
CenterLowerArmR = HLowerArmRtoBase(1:3,4);
ptsLowerArmR = makeCylBodyPart(armLowerRadius,armLowerRadius,armLowerLength,CenterLowerArmR,RLowerArmR,uniqueCylinderPts);
normalsLowerArmR = RLowerArmR*uniqueCylinderNormals;

% left hand
ptsHandL=uniqueCubiclePtsHands*0;
% scale
ptsHandL(:,1)=uniqueCubiclePtsHands(:,1)*handWidth;
ptsHandL(:,2)=uniqueCubiclePtsHands(:,2)*handDepth;
ptsHandL(:,3)=uniqueCubiclePtsHands(:,3)*handLength;
RMatHandL = angle2dcm(params(11),params(12),params(13));
HWristLToLowerArmL = [RMatHandL [0;0;-armLowerLength/2]; 0 0 0 1];
HHandLtoBase = HLowerArmLtoBase*HWristLToLowerArmL*HHandCtrToWrist;
RHandL = HHandLtoBase(1:3,1:3);
CenterHandL = HHandLtoBase(1:3,4);
% rotation
ptsHandL = RHandL*ptsHandL';
% translation
ptsHandL=ptsHandL+repmat(CenterHandL,1,size(ptsHandL,2));
normalsHandL = RHandL*uniqueCubicleNormalsHands;

% right hand
ptsHandR=uniqueCubiclePtsHands*0;
% scale
ptsHandR(:,1)=uniqueCubiclePtsHands(:,1)*handWidth;
ptsHandR(:,2)=uniqueCubiclePtsHands(:,2)*handDepth;
ptsHandR(:,3)=uniqueCubiclePtsHands(:,3)*handLength;
RMatHandR = angle2dcm(params(18),params(19),params(20));
HWristRToLowerArmR = [RMatHandR [0;0;-armLowerLength/2]; 0 0 0 1];
HHandRtoBase = HLowerArmRtoBase*HWristRToLowerArmR*HHandCtrToWrist;
RHandR = HHandRtoBase(1:3,1:3);
CenterHandR = HHandRtoBase(1:3,4);
% rotation
ptsHandR = RHandR*ptsHandR';
% translation
ptsHandR=ptsHandR+repmat(CenterHandR,1,size(ptsHandR,2));
normalsHandR = RHandR*uniqueCubicleNormalsHands;

% LEGS
% left upper leg
RMatUpperLegL = angle2dcm(params(21),params(22),params(23));
HHipLToAbd = [RMatUpperLegL [abdomenRadius1-legUpperRadius;0;-abdomenLength/2]; 0 0 0 1];
HUpperLegLToBase = HAbd*HHipLToAbd*HLegCtrToHip;
RUpperLegL = HUpperLegLToBase(1:3,1:3);
CenterUpperLegL = HUpperLegLToBase(1:3,4);
ptsUpperLegL = makeCylBodyPart(legUpperRadius,legUpperRadius,legUpperLength,CenterUpperLegL,RUpperLegL,uniqueCylinderPts);
normalsUpperLegL = RUpperLegL*uniqueCylinderNormals;

% right upper leg
RMatUpperLegR = angle2dcm(params(27),params(28),params(29));
HHipRToAbd = [RMatUpperLegR [-abdomenRadius1+legUpperRadius;0;-abdomenLength/2]; 0 0 0 1];
HUpperLegRToBase = HAbd*HHipRToAbd*HLegCtrToHip;
RUpperLegR = HUpperLegRToBase(1:3,1:3);
CenterUpperLegR = HUpperLegRToBase(1:3,4);
ptsUpperLegR = makeCylBodyPart(legUpperRadius,legUpperRadius,legUpperLength,CenterUpperLegR,RUpperLegR,uniqueCylinderPts);
normalsUpperLegR = RUpperLegR*uniqueCylinderNormals;

% left lower leg
RMatLowerLegL = angle2dcm(0,0,params(24));
HElbowLToUpperLegL = [RMatLowerLegL [0;0;-legUpperLength/2]; 0 0 0 1];
HLowerLegLtoBase = HUpperLegLToBase*HElbowLToUpperLegL*HLowerLegCtrToElbow;
RLowerLegL = HLowerLegLtoBase(1:3,1:3);
CenterLowerLegL = HLowerLegLtoBase(1:3,4);
ptsLowerLegL = makeCylBodyPart(legLowerRadius,legLowerRadius,legLowerLength,CenterLowerLegL,RLowerLegL,uniqueCylinderPts);
normalsLowerLegL = RLowerLegL*uniqueCylinderNormals;

% right lower leg
RMatLowerLegR = angle2dcm(0,0,params(30));
HElbowRToUpperLegR = [RMatLowerLegR [0;0;-legUpperLength/2]; 0 0 0 1];
HLowerLegRtoBase = HUpperLegRToBase*HElbowRToUpperLegR*HLowerLegCtrToElbow;
RLowerLegR = HLowerLegRtoBase(1:3,1:3);
CenterLowerLegR = HLowerLegRtoBase(1:3,4);
ptsLowerLegR = makeCylBodyPart(legLowerRadius,legLowerRadius,legLowerLength,CenterLowerLegR,RLowerLegR,uniqueCylinderPts);
normalsLowerLegR = RLowerLegR*uniqueCylinderNormals;

% left foot
ptsFootL=uniqueCubiclePts*0;
% scale
ptsFootL(:,1)=uniqueCubiclePts(:,1)*footWidth;
ptsFootL(:,2)=uniqueCubiclePts(:,2)*footLength;
ptsFootL(:,3)=uniqueCubiclePts(:,3)*footDepth;
RMatFootL = angle2dcm(params(25),0,params(26));
HAnkleLToLowerLegL = [RMatFootL [0;0;-legLowerLength/2]; 0 0 0 1];
HFootLtoBase = HLowerLegLtoBase*HAnkleLToLowerLegL*HFootCtrToAnkle;
RFootL = HFootLtoBase(1:3,1:3);
CenterFootL = HFootLtoBase(1:3,4);
% rotation
ptsFootL = RFootL*ptsFootL';
% translation
ptsFootL=ptsFootL+repmat(CenterFootL,1,size(ptsFootL,2));
normalsFootL = RFootL*uniqueCubicleNormals;

% right foot
ptsFootR=uniqueCubiclePts*0;
% scale
ptsFootR(:,1)=uniqueCubiclePts(:,1)*footWidth;
ptsFootR(:,2)=uniqueCubiclePts(:,2)*footLength;
ptsFootR(:,3)=uniqueCubiclePts(:,3)*footDepth;
RMatFootR = angle2dcm(params(31),0,params(32));
HAnkleRToLowerLegR = [RMatFootR [0;0;-legLowerLength/2]; 0 0 0 1];
HFootRtoBase = HLowerLegRtoBase*HAnkleRToLowerLegR*HFootCtrToAnkle;
RFootR = HFootRtoBase(1:3,1:3);
CenterFootR = HFootRtoBase(1:3,4);
% rotation
ptsFootR = RFootR*ptsFootR';
% translation
ptsFootR=ptsFootR+repmat(CenterFootR,1,size(ptsFootR,2));
normalsFootR = RFootR*uniqueCubicleNormals;

% concatenate body part point clouds
HumanModelVert = [ptsAbdomen ptsHead ptsNeck ptsUpperArmL ptsUpperArmR ptsLowerArmL ptsLowerArmR ptsHandL ptsHandR ptsUpperLegL ptsUpperLegR ptsLowerLegL ptsLowerLegR ptsFootL ptsFootR];
HumanModelNormals = [normalsAbdomen normalsHead normalsNeck normalsUpperArmL normalsUpperArmR normalsLowerArmL normalsLowerArmR normalsHandL normalsHandR normalsUpperLegL normalsUpperLegR normalsLowerLegL normalsLowerLegR normalsFootL normalsFootR];
end

function ptsCylBodyPart = makeCylBodyPart(Radius1,Radius2,Length,center,R, uniqueCylinderPts)
% ptsCylBodyPart = makeCylBodyPart(Radius1,Radius2,Length,center,R, uniqueCylinderPts)

% scale
x=uniqueCylinderPts(:,1)*Radius1;
y=uniqueCylinderPts(:,2)*Radius2;
z=uniqueCylinderPts(:,3)*Length;
% rotation
ptsCylBodyPart = R*[x y z]';
% translation
ptsCylBodyPart=ptsCylBodyPart+repmat(center,1,size(ptsCylBodyPart,2));
end