% figure
hold on
HumanModelVert=updateHumanModel(x);
plot3(HumanModelVert(1,:),HumanModelVert(2,:),HumanModelVert(3,:),'k*')
plot3(XBedRegion(:),YBedRegion(:),ZBedRegion(:),'r.')
% Xs=pc(:,:,1);
% X=X(:);
% Ys=pc(:,:,2);
% Y=Y(:);
% Zs=-pc(:,:,3);
% Zs(Zs==0)=NaN;
% surf(Xs,Ys,Zs,'edgecolor','none')
hold off
axis vis3d image
view(3)
lighting gouraud
camlight