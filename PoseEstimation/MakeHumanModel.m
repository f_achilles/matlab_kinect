% Make simple human model

% body properties
% lengths in [mm]
headRadius = 100; %sphere

neckLength = 50; %cylinder
neckRadius = 70;

abdomenLength = 600; %ellipticCylinder
abdomenRadius1 = 175;
abdomenRadius2 = 75;

armUpperLength = 300; %cylinders
armUpperRadius = 50;
armLowerLength = 300;
armLowerRadius = 40;

handLength = 150; %cubicle
handWidth = 90;
handDepth = 30;

legUpperLength = 450; %cylinders
legUpperRadius = 75;
legLowerLength = 400;
legLowerRadius = 50;

footLength = 200; %cubicle
footWidth = 90;
footDepth = 50;

% rendering properties
pointsPerCircle = 20; % for cylinders and spheres

% compute height
fullHeight = footDepth + legLowerLength + legUpperLength +...
    abdomenLength + neckLength + headRadius*2;
disp(['The SimpleHumanModel has a height of ' num2str(fullHeight/10) 'cm.'])

% make head
headCenterHeight = fullHeight - headRadius;
[xh,yh,zh]=sphere(pointsPerCircle);
xh=xh*headRadius;
yh=yh*headRadius;
zh=zh*headRadius;

zh=zh+headCenterHeight;

% make neck
neckCenterHeight = fullHeight - 2*headRadius - neckLength/2;
[xn,yn,zn]=cylinder(1,pointsPerCircle); %standard cylinder is centered at x=y=0, z=0.5
zn=zn-0.5;

xn=xn*neckRadius;
yn=yn*neckRadius;

zn=zn*neckLength+neckCenterHeight;

% make abdomen
abdomenCenterHeight = fullHeight - 2*headRadius - neckLength - abdomenLength/2;
[xa,ya,za]=cylinder(1,pointsPerCircle); %standard cylinder is centered at x=y=0, z=0.5
za=za-0.5;

xa=xa*abdomenRadius1;
ya=ya*abdomenRadius2;

za=za*abdomenLength+abdomenCenterHeight;

% make arms
armUpperCenterHeight = fullHeight - 2*headRadius - neckLength - armUpperLength/2;
armLowerCenterHeight = fullHeight - 2*headRadius - neckLength - armUpperLength - armLowerLength/2;
[xarmup,yarmup,zarmup]=cylinder(1,pointsPerCircle); %standard cylinder is centered at x=y=0, z=0.5
zarmup=zarmup-0.5;

xarmupL=xarmup*armUpperRadius+abdomenRadius1+armUpperRadius;
yarmupL=yarmup*armUpperRadius;
zarmupL=zarmup*armUpperLength+armUpperCenterHeight;

xarmupR=xarmup*armUpperRadius-abdomenRadius1-armUpperRadius;
yarmupR=yarmup*armUpperRadius;
zarmupR=zarmup*armUpperLength+armUpperCenterHeight;

[xarmlow,yarmlow,zarmlow]=cylinder(1,pointsPerCircle); %standard cylinder is centered at x=y=0, z=0.5
zarmlow=zarmlow-0.5;

xarmlowL=xarmlow*armLowerRadius+abdomenRadius1+armUpperRadius;
yarmlowL=yarmlow*armLowerRadius;
zarmlowL=zarmlow*armLowerLength+armLowerCenterHeight;

xarmlowR=xarmlow*armLowerRadius-abdomenRadius1-armUpperRadius;
yarmlowR=yarmlow*armLowerRadius;
zarmlowR=zarmlow*armLowerLength+armLowerCenterHeight;

% make hands
handCenterHeight = fullHeight - 2*headRadius - neckLength - armUpperLength - armLowerLength - handLength/2;
xha=[-0.5 0.5 0.5 -0.5; -0.5 0.5 0.5 -0.5];
yha=[-0.5 -0.5 0.5 0.5; -0.5 -0.5 0.5 0.5];
zha=[-0.5 -0.5 -0.5 -0.5; 0.5 0.5 0.5 0.5];

xhL=xha*handWidth+abdomenRadius1+armUpperRadius;
yhL=yha*handDepth;
zhL=zha*handLength+handCenterHeight;

xhR=xha*handWidth-abdomenRadius1-armUpperRadius;
yhR=yha*handDepth;
zhR=zha*handLength+handCenterHeight;

% make legs
legUpperCenterHeight = footDepth + legLowerLength + legUpperLength/2;
legLowerCenterHeight = footDepth + legLowerLength/2;
[xlegup,ylegup,zlegup]=cylinder(1,pointsPerCircle); %standard cylinder is centered at x=y=0, z=0.5
zlegup=zlegup-0.5;

xlegupL=xlegup*legUpperRadius+abdomenRadius1-legUpperRadius;
ylegupL=ylegup*legUpperRadius;
zlegupL=zlegup*legUpperLength+legUpperCenterHeight;

xlegupR=xlegup*legUpperRadius-abdomenRadius1+legUpperRadius;
ylegupR=ylegup*legUpperRadius;
zlegupR=zlegup*legUpperLength+legUpperCenterHeight;

[xleglow,yleglow,zleglow]=cylinder(1,pointsPerCircle); %standard cylinder is centered at x=y=0, z=0.5
zleglow=zleglow-0.5;

xleglowL=xleglow*legLowerRadius+abdomenRadius1-legUpperRadius;
yleglowL=yleglow*legLowerRadius;
zleglowL=zleglow*legLowerLength+legLowerCenterHeight;

xleglowR=xleglow*legLowerRadius-abdomenRadius1+legUpperRadius;
yleglowR=yleglow*legLowerRadius;
zleglowR=zleglow*legLowerLength+legLowerCenterHeight;

% make feet
footCenterHeight = footDepth/2;
% xha=[-0.5 0.5 0.5 -0.5; -0.5 0.5 0.5 -0.5];
% yha=[-0.5 -0.5 0.5 0.5; -0.5 -0.5 0.5 0.5];
% zha=[-0.5 -0.5 -0.5 -0.5; 0.5 0.5 0.5 0.5];

xfL=xha*footWidth+abdomenRadius1-legUpperRadius;
yfL=yha*footLength-(footLength/2-legLowerRadius);
zfL=zha*footDepth+footCenterHeight;

xfR=xha*footWidth-abdomenRadius1+legUpperRadius;
yfR=yha*footLength-(footLength/2-legLowerRadius);
zfR=zha*footDepth+footCenterHeight;

% plot body
figure
hold on
% head
surf(xh,yh,zh,'facecolor','red')
% neck
surf(xn,yn,zn,'facecolor','green')
% abdomen
surf(xa,ya,za,'facecolor','blue')
% arms
surf(xarmupL,yarmupL,zarmupL,'facecolor','red')
surf(xarmupR,yarmupR,zarmupR,'facecolor','green')
surf(xarmlowL,yarmlowL,zarmlowL,'facecolor','blue')
surf(xarmlowR,yarmlowR,zarmlowR,'facecolor','red')
% hands
surf(xhL,yhL,zhL,'facecolor','green')
surf(xhR,yhR,zhR,'facecolor','blue')
% legs
surf(xlegupL,ylegupL,zlegupL,'facecolor','red')
surf(xlegupR,ylegupR,zlegupR,'facecolor','green')
surf(xleglowL,yleglowL,zleglowL,'facecolor','blue')
surf(xleglowR,yleglowR,zleglowR,'facecolor','red')
% feet
surf(xfL,yfL,zfL,'facecolor','green')
surf(xfR,yfR,zfR,'facecolor','blue')
hold off
view(3)
axis image vis3d off
lighting phong
camlight headlight
