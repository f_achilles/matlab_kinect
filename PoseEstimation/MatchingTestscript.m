%% test script for Human Model Matching

% 1) load test data (100 sample frames)
% 2) convert depth to XYZ pointcloud
% 3) match model, save final result pose and screenshot of the plot
% 4) output information about current sample number and fitting error

% 1)
load('testSamplesNoCover.mat')

for sampleIndex = 1:100
    disp(['Sample image number ' num2str(sampleIndex) ' is being processed.'])
    tic
    % 2)
    D=testSamplesNoCover(:,:,sampleIndex);
    pc=zeros([size(D) 3]);
    W=size(D,2);
    H=size(D,1);
    f=5.453;
    for indWidth = 1:W
        for indHeight= 1:H
            % copy z value
            pc(indHeight,indWidth,3)=-double(D(indHeight,indWidth));
            % calc x value
            pc(indHeight,indWidth,1)=(pc(indHeight,indWidth,3)/f)*...
                ((indWidth-W/2)*0.0093+0.063);
            % calc y value
            pc(indHeight,indWidth,2)=(pc(indHeight,indWidth,3)/f)*...
                ((indHeight-H/2)*0.0093+0.039);
        end
    end
    X=pc(:,:,1);
    Y=pc(:,:,2);
    Z=pc(:,:,3);
    Z(Z==0)=NaN;
    Z=Z(:);
    validIndices=find(~isnan(Z));
    X=X(validIndices);
    Y=Y(validIndices);
    Z=Z(validIndices);
    % 3)
    [matchedModelParameters, error]=FitHumanToDepth([X Y Z]);
    % 4)
    toc
    disp(['Matching error of to sample image number ' num2str(sampleIndex) ' was ' num2str(error) 'units.'])
end

