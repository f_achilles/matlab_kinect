% plot human model

% parameters for human model:
% 1: Xcenter
% 2: Ycenter
% 3: Zcenter
% 4: Yawcenter
% 5: Pitchcenter
% 6: Rollcenter
% 7: YawUpperArmL
% 8: PitchUpperArmL
% 9: RollUpperArmL
%10: PhiLowerArmL
%11: YawHandL
%12: PitchHandL
%13: RollHandL
%14: YawUpperArmR
%15: PitchUpperArmR
%16: RollUpperArmR
%17: PhiLowerArmR
%18: YawHandR
%19: PitchHandR
%20: RollHandR
%21: YawUpperLegL
%22: PitchUpperLegL
%23: RollUpperLegL
%24: PhiLowerLegL
%25: YawFootL
%26: RollFootL
%27: YawUpperLegR
%28: PitchUpperLegR
%29: RollUpperLegR
%30: PhiLowerLegR
%31: YawFootR
%32: RollFootR

params = [...
    0 % PosAbdomenCenter
    0
    2
    0 % RotAbdomenCenter
    .5
    0
    1 % RotLeftShoulder
    1
    1
    0.7 % RotLeftElbow
    0.2 % RotLeftHand
    0.2
    0.2
    0.5 % RotRightShoulder
    0
    0.5
    0.7 % RotRightElbow
    -0.2 % RotRightHand
    -0.2
    -0.2
    1 % RotLeftHip
    1
    1
    -0.7 % RotLeftKnee
    0.2 % RotLeftFoot
    0.2
    0.5 % RotRightHip
    0
    0.5
    -0.7 % RotRightKnee
    -0.2 % RotRightFoot
    -0.2
];
% params = zeros(32,1);
params(3)=1.175;
% testing single joints
% % Elbows: Result: The elbows are initialized pointing to the back, they rotate about the
% % negative X axis!!
% params(10)=pi/2;
% params(17)=pi/2;
% % Knees: Result: The knees are initialized pointing to the front, they rotate about the
% % negative X axis!!
% params(24)=-pi/2;
% params(30)=-pi/2;

% TIME MEASUREMENT, RESULT: 3.5ms per raw creation, might go to 2.5ms when
% using static variables
% tic
% for i=1:1000
[HumanModelVert, HumanModelNormals] = updateHumanModel(params);
% end
% CreationTime=toc;
% disp(['Raw creation time is ' num2str(CreationTime/1000) ' seconds.'])

figure
plot3(HumanModelVert(1,:),HumanModelVert(2,:),HumanModelVert(3,:),'k*')
xlabel('X-axis')
ylabel('Y-axis')
zlabel('Z-axis')
axis vis3d image


