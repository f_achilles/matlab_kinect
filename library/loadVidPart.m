function loadedData = loadVidPart(dataDir)
% funciton for loading a .mat file with user input
    matFiles = dir([dataDir '/*.avi']);
    if numel(matFiles)~=0
        disp('Choose the recording to be replayed')
        for iFiles = 1:numel(matFiles)
            disp([num2str(iFiles) ': ' matFiles(iFiles).name])
        end
    else
        error('no .avi files found in current folder')
    end
    indFile = input('Input valid number and press enter: ');
    
    kinectMovie = VideoReader([dataDir '/' matFiles(indFile).name]);
    
    frameStart = input('Input number of -first Frame- and press enter: ');
    frameEnd = input('Input number of -last Frame- and press enter: ');
    
    if ~isnumeric([frameStart frameEnd])
        error('start or end frame invalid')
    else
        loadedData = read(kinectMovie, [frameStart frameEnd]);
    end

end