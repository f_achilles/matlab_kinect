function RMSDistance = NURBSdistanceRMS(ctrl,ptCloudPts,nurbsObj)
    
    persistent uvStore;
    firstcall = false;
    numPtCloudPts = size(ptCloudPts,1);   
    if isempty(uvStore)
        uvStore = cell(1,numPtCloudPts);
        firstcall = true;
    end
    sqDistances = zeros(numPtCloudPts,1);
    
    zControlPts = ones(nurbsObj.number);
    zControlPts(1:end) = ctrl;
    
    newctrlPts = nurbsObj.coefs;
    newctrlPts(3,:,:) = permute(zControlPts,[3 1 2]);
    
    nurbsObj = nrbmak(newctrlPts, nurbsObj.knots);
    nurbsObjDeriv = nrbderiv(nurbsObj);
    
    options = optimoptions('lsqnonlin','Jacobian','on','Display','off');
    
    if firstcall
    for ptIdx = 1:numPtCloudPts
        % find the closest point on the NURBS, store the distance
        u=0.5;v=0.5;
        uvInit = [u;v];
        fun=@(x)Dist(x,ptCloudPts(ptIdx,:),nurbsObj,nurbsObjDeriv);
        [uvStore{ptIdx},sqDistances(ptIdx)]=lsqnonlin(fun,uvInit,[0;0],[1;1], options); %bounds for u and v
    end
    else
        for ptIdx = 1:numPtCloudPts
            % find the closest point on the NURBS, store the distance
            uvInit = uvStore{ptIdx};
            fun=@(x)Dist(x,ptCloudPts(ptIdx,:),nurbsObj,nurbsObjDeriv);
            [uvStore{ptIdx},sqDistances(ptIdx)]=lsqnonlin(fun,uvInit,[0;0],[1;1], options); %bounds for u and v
        end
    end
    RMSDistance = double(sum(sqDistances)/numPtCloudPts);
end

function [ance, jacobianMat] = Dist(uv,pt,nurbsObj,nurbsObjDeriv)
[p] = nrbeval(nurbsObj,{uv(1),uv(2)});
ance = double(p-pt');
if nargout > 1
    [~,jacobianMat] = nrbdeval(nurbsObj,nurbsObjDeriv,{uv(1),uv(2)});
    jacobianMat = [jacobianMat{1} jacobianMat{2}];
end
end