function [RMSDistance, jacobianOfDistances]= NURBSdistanceForLmSolver(ctrl,ptCloudPts,nurbsObj)
    jacobianOfDistances = [];
    
    persistent uvStore upperBd lowerBd;
    firstcall = false;
%     numPtCloudPts = size(ptCloudPts,2)*size(ptCloudPts,3);
    numPtCloudPts = size(ptCloudPts,1);
    if isempty(uvStore)
        uvStore = 0.5*ones(2*numPtCloudPts,1);
        upperBd = ones(2*numPtCloudPts,1);
        lowerBd = zeros(2*numPtCloudPts,1);
        firstcall = true;
    end
        
    zControlPts = ones(nurbsObj.number);
    zControlPts(1:end) = ctrl;
    
    newctrlPts = nurbsObj.coefs;
    newctrlPts(3,:,:) = permute(zControlPts,[3 1 2]);
    
    % create updated NURBS
    nurbsObj = nrbmak(newctrlPts, nurbsObj.knots);
    % create derivative of nurbs
    nurbsObjDeriv = nrbderiv(nurbsObj);
    
    options = optimoptions('lsqnonlin','Jacobian','on','Display','off');
    
    if firstcall
        % find the closest point on the NURBS, store the distance
        fun=@(x)DistAll(x,ptCloudPts,nurbsObj,nurbsObjDeriv);
        [uvStore,sqDistances]=lsqnonlin(fun,uvStore,lowerBd,upperBd, options); %bounds for u and v
    else
        % find the closest point on the NURBS, store the distance
        fun=@(x)DistAll(x,ptCloudPts,nurbsObj,nurbsObjDeriv);
        [uvStore,sqDistances]=lsqnonlin(fun,uvStore,lowerBd,upperBd, options); %bounds for u and v
    end
    RMSDistance = double(sum(sqDistances)/numPtCloudPts);
end

function [ance, jacobianMat] = DistAll(uv,pt,nurbsObj,nurbsObjDeriv)
[NURBpts] = nrbeval(nurbsObj,[uv(1:2:end)';uv(2:2:end)']);
ance = double(NURBpts-pt');
if nargout > 1
    [~,jacobianMat] = nrbdeval(nurbsObj,nurbsObjDeriv,[uv(1:2:end)';uv(2:2:end)']);
    jacobianMat = [jacobianMat{:}];
end
end