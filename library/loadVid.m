function loadedData = loadVid(dataDir)
% funciton for loading a .mat file with user input
    matFiles = dir([dataDir '/*.avi']);
    if numel(matFiles)~=0
        disp('Choose the recording to be replayed')
        for iFiles = 1:numel(matFiles)
            disp([num2str(iFiles) ': ' matFiles(iFiles).name])
        end
    else
        error('no .avi files found in current folder')
    end
    indFile = input('Input valid number and press enter: ');
    
    kinectMovie = VideoReader([dataDir '/' matFiles(indFile).name]);
    loadedData = read(kinectMovie);
%     
%     loadedDataStruct = load([dataDir '/' matFiles(indFile).name]);
%     varname=whos('-file',[dataDir '/' matFiles(indFile).name]);
%     varname=varname.name;
%     loadedData=loadedDataStruct.(varname);

end