function normalsOfPts = calcNormalsWithPCA(kdTreeObject, KinectViewPoint)
distanceThreshold = 50; % 5cm radius for normals calculation
leastNumberOfNeighbors = 10;
rangeIndices = rangesearch(kdTreeObject,kdTreeObject.X,distanceThreshold);
normalsOfPts=0*kdTreeObject.X;
for i=1:numel(rangeIndices)
    if numel(rangeIndices{i})>=leastNumberOfNeighbors
        [pcVectors]=pca(kdTreeObject.X(rangeIndices{i},:));
        normalsOfPts(i,:)=(pcVectors(:,3))';
        if dot(KinectViewPoint'-kdTreeObject.X(i,:),normalsOfPts(i,:)) < 0
            normalsOfPts(i,:)=-normalsOfPts(i,:);
        end
    else
        normalsOfPts(i,:)=NaN;
    end
end

end