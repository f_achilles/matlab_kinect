function [loadedData,indFile] = loadImage(dataDir)
% funciton for loading a .mat file with user input
    matFiles = dir([dataDir '/*.png']);
    if numel(matFiles)~=0
        disp('Choose the recording to be replayed')
        for iFiles = 1:numel(matFiles)
            disp([num2str(iFiles) ': ' matFiles(iFiles).name])
        end
    else
        error('no .png files found in current folder')
    end
    indFile = input('Input valid number and press enter: ');
    loadedData = imread([dataDir '/' matFiles(indFile).name]);

end