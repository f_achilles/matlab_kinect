%% make point cloud out of Kinect depth data in pixel grid
% formulas for X[mm] and Y[mm]:
% X = -(Z/f)*(x-x0+dx)
% Y = -(Z/f)*(y-y0+dy)
% where f is focal length
% x0,y0 are principal point offsets
% dx,dy are lens distortion coefficients
% Values:
% f=5.453mm
% x=(ix-W/2)*0.0093mm, y=(iy-H/2)*0.0093mm
% x0=-0.063mm, y0=-0.039mm
% 0mm  < dx,dy < 0.020mm
%
% All formulas and values from:
% Khoshelham, K., & Elberink, S. O. (2012).
% Accuracy and resolution of Kinect depth data for indoor mapping applications.
% Sensors (Basel, Switzerland), 12(2), 1437�54. doi:10.3390/s120201437
% load('janCroppedFrameFive.mat')
load('janFrameThousand.mat')
pc=zeros([size(D) 3],'single');
W=size(D,2);
H=size(D,1);
f=5.453;
for indWidth = 1:W
    for indHeight= 1:H
        % copy z value
        pc(indHeight,indWidth,3)=-single(D(indHeight,indWidth));
        % calc x value
        pc(indHeight,indWidth,1)=(pc(indHeight,indWidth,3)/f)*...
            ((indWidth-W/2)*0.0093+0.063);
        % calc y value
        pc(indHeight,indWidth,2)=(pc(indHeight,indWidth,3)/f)*...
            ((indHeight-H/2)*0.0093+0.039);
    end
end
X=pc(:,:,1);
Y=pc(:,:,2);
Z=pc(:,:,3);
Z(Z==0)=NaN;

% remove annoying connections in surf-plot
diffZx = abs([diff(Z,1,2) zeros(H,1)]);
diffZy = abs([diff(Z,1,1); zeros(1,W)]);
edgeMask = diffZx>200 | diffZy>200;
edgeMask = imdilate(edgeMask,ones(3));

ZforSurfPlot = Z;
ZforSurfPlot(edgeMask)=NaN;

figure
patientSurface=surf(double(X),double(Y),double(ZforSurfPlot),'edgecolor','none','facecolor','interp');%,'alphadata',double(~edgeMask));
lighting gouraud
camlight
axis image
axis vis3d
xlabel('X axis')
ylabel('Y axis')
zlabel('Z axis')

clear ZforSurfPlot edgeMask diffZx diffZy

Z=Z(:);
validIndices=find(~isnan(Z));
X=X(validIndices);
Y=Y(validIndices);
Z=Z(validIndices);

% plot figure to visualize edges in the picture
% figure
% imagesc(edgeMask)
% set(gca,'XDir','reverse')
% axis image



