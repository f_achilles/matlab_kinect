% make AVI video from depthContainer or clusterContainer

nameOfContainer = input('Which height x width x NumFrames variable should be converted to a video?\n','s');

nameOfColormap = input('Which colormap do you want to use?\n','s');

colorBorders = input('What should be cmin and cmax for coloring?\n(separate by space, no brackets)\n','s');

if isempty(colorBorders)
    caxis auto
else
    cminmax = sscanf(colorBorders,'%f %f');
    caxis(cminmax')
end

videoContainer = evalin('base',nameOfContainer);

vidObj = VideoWriter('VideoPrev.avi','MPEG-4');
% vidObj.LosslessCompression = true;

open(vidObj);

image(videoContainer(:,:,1));

colormap(nameOfColormap);

for i=1:size(videoContainer,3)
    imagesc(videoContainer(:,:,i));
    axis image
    caxis(cminmax')
    writeVideo(vidObj,getframe);
end

close(vidObj);

clear videoContainer nameOfContainer nameOfColormap