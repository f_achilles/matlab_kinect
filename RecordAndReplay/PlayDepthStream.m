clear all, close all
%% replay Kinect depth stream
% load recorded depth stream
dataDir='D:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings\allLabelledParts';
matFiles = dir([dataDir '/*.mat']);
if numel(matFiles)~=0
    disp('Choose the recording to be replayed')
    for iFiles = 1:numel(matFiles)
        disp([num2str(iFiles) ': ' matFiles(iFiles).name])
    end
else
    error('no .mat files found in current folder')
end
indFile = input('Input valid number and press enter: ');
load([dataDir '/' matFiles(indFile).name])
varname=whos('-file',[dataDir '/' matFiles(indFile).name]);
varname=varname.name;
depthContainer = evalin('base',varname);
if ~strcmp(varname, 'depthContainer')
    clear(varname);
end
% plot slected recording
numFrames = size(depthContainer,3);
% D = double(depthContainer(:,:,1));
D = depthContainer(:,:,1);
% D(D<1)=NaN;
figure;
hD=imshow(D,[0 5000]);
% view(525.2701,88)
% hD=surf(-D,'edgecolor','none');
% axis([1 640 1 480 -5000 0])
% axis image vis3d
ratio = floor(5000/256);
cubeMap = colormap('jet');
betterCubeMap = repmat(cubeMap,ratio,1);
colormap(betterCubeMap);
% set(hD,'Cdatamapping','direct')
for i=1:numFrames
%     D=double(depthContainer(:,:,i));
    D=depthContainer(:,:,i);
%     D(D<1)=NaN;
    set(hD,'CDATA',D);
%     set(hD,'ZData',-D);
    drawnow;
%     axis image vis3d
%     view(525.2701,88)
    pause(0.05)
end

