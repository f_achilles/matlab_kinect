clear all, close all
%% replay Kinect depth and video streams
% load recorded depth stream
matFiles = dir('./*.mat');
if numel(matFiles)~=0
    disp('Choose the recording to be replayed')
    for iFiles = 1:numel(matFiles)
        disp([num2str(iFiles) ': ' matFiles(iFiles).name])
    end
else
    error('no .mat files found in current folder') 
end
indFile = input('Input valid number and press enter: ');
load(matFiles(indFile).name)
varname=whos('-file',matFiles(indFile).name);
varname=varname.name;
depthContainer = evalin('base',varname);
clear(varname);
if numel(dir([matFiles(indFile).name(1:(end-4)) '.avi']))~=0
    kinectMovie = VideoReader([matFiles(indFile).name(1:(end-4)) '.avi']);
else
    warning('No RGB video found, only depth stream is replayed.')
end

% plot selected recording
[monitorSize] = get(0,'MonitorPositions');
monitorWidth = monitorSize(3);
monitorHeight = monitorSize(4);
borderPixels = 20;

figure('position',[floor(monitorWidth/2)-640-borderPixels floor(monitorHeight/2)-240-borderPixels 2*borderPixels+2*640 2*borderPixels+480]);
subplot(1,2,1);
D = depthContainer(:,:,1);
hD=imshow(D,[0 5000]);
subplot(1,2,2);
if exist('kinectMovie','var')
    I=read(kinectMovie,1);
else
    I=importdata('XboxKinect2.jpg');
end
hI=imshow(I);

ratio = floor(5000/256);
cubeMap = colormap('jet');
betterCubeMap = repmat(cubeMap,ratio,1);
colormap(betterCubeMap);

numFrames = size(depthContainer,3);
for i=1:numFrames
    D=depthContainer(:,:,i);
    set(hD,'CDATA',D);
    if exist('kinectMovie','var')
        I=read(kinectMovie,i);
        set(hI,'CDATA',I);
    end
    drawnow;
    pause%(0.03)
end

