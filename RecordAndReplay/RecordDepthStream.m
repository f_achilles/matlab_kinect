clear all; close all
%% record and display Kinect depth stream
% set number of frames to be recorded
numFrames = 300;


% Start the Kinect Process
KinectHandles=mxNiCreateContext();

figure;
D=mxNiDepth(KinectHandles); D=permute(D,[2 1]);
hD=imshow(D,[0 9000]); colormap('colorcube');

depthContainer = zeros(480,640,numFrames,'uint16');
for i=1:numFrames
    D=mxNiDepth(KinectHandles); D=permute(D,[2 1]);
    set(hD,'CDATA',D);
    drawnow;
    depthContainer(:,:,i)=D;
end

% Stop the Kinect Process
mxNiDeleteContext(KinectHandles);

filename = input('Input name for recording and press enter: ');
save(filename, 'depthContainer')