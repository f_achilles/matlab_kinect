clear all; close all
%% record and display both Kinect streams
% set number of frames to be recorded
numFrames = 7200;

% Start the Kinect Process
if ~exist('KinectHandles','var')
KinectHandles=mxNiCreateContext();
end

[monitorSize] = get(0,'MonitorPositions');
monitorWidth = monitorSize(3);
monitorHeight = monitorSize(4);

borderPixels = 20;
figure('position',[floor(monitorWidth/2)-640-borderPixels floor(monitorHeight/2)-240-borderPixels 2*borderPixels+2*640 2*borderPixels+480]);
% D=mxNiDepth(KinectHandles); D=permute(D,[2 1]);
D=mxNiDepth(KinectHandles); D=rot90(D,2);
% I=mxNiPhoto(KinectHandles); I=permute(I,[3 2 1]);
I=mxNiPhoto(KinectHandles); I=permute(I,[2 3 1]);
I(:,:,1)=rot90(I(:,:,1),2);
I(:,:,2)=rot90(I(:,:,2),2);
I(:,:,3)=rot90(I(:,:,3),2);
subplot(1,2,1)
hD=imshow(D,[0 9000]); colormap('colorcube');
subplot(1,2,2);
hI=imshow(I);

% depthContainer = zeros(480,640,numFrames,'uint16');
depthContainer = zeros(640,480,numFrames,'uint16');
RGBVideo = VideoWriter('tmpRGBVideo.avi');
open(RGBVideo)
for i=1:numFrames
%     D=mxNiDepth(KinectHandles); D=permute(D,[2 1]);
    D=mxNiDepth(KinectHandles); D=rot90(D,2);
%     I=mxNiPhoto(KinectHandles); I=permute(I,[3 2 1]);
    I=mxNiPhoto(KinectHandles); I=permute(I,[2 3 1]);
    I(:,:,1)=rot90(I(:,:,1),2);
    I(:,:,2)=rot90(I(:,:,2),2);
    I(:,:,3)=rot90(I(:,:,3),2);
    set(hD,'CDATA',D);
    set(hI,'CDATA',I);
    drawnow;
    depthContainer(:,:,i)=D;
    writeVideo(RGBVideo,struct('cdata',I,'colormap',[]));
end
close(RGBVideo)

% Stop the Kinect Process
% mxNiDeleteContext(KinectHandles);

filename = input('Input name for recording and press enter: ');
save(filename, 'depthContainer','-v7.3')
movefile('tmpRGBVideo.avi', [filename  '.avi'])
