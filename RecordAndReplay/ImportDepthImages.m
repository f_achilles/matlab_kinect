%% import labelled range image pairs as .png with grey colormap

% run the import script with user input
sourceFolder = './data/imagePairs';
[importedImage,indFile] = loadImage(sourceFolder);

% crop to obtain left side (labelled depth)
importedImage = importedImage(:,1:(size(importedImage,2)/2),:);

% class label   [R G B]
%---
% head:         [237 28 36]
% abdomen:      [136  0 21]
% upper arms:   [ 34 177 76]
% lower arms:   [181 230 29]
% hands:        [255 242  0]
% blanket:      [ 63 72 204]
% mattress:     [163  73 164]
% cushion:      [153 217 234]
% floor:        [185 122  87]
% wall:         [255 174 201]

% not what I want: headMask = bwconvhull(headMask);
% what i want: region grow outside the labelleld object! :)
mex 'C:\Users\Felix\Documents\MATLAB\RegionGrowing\RegionGrowing_mex.cpp'

headMask = importedImage(:,:,1)==237 & importedImage(:,:,2)==28 & importedImage(:,:,3)==36;
headMask = ~RegionGrowing(headMask,0.5,[1 1]);

abdomenMask = importedImage(:,:,1)==136 & importedImage(:,:,2)==0 & importedImage(:,:,3)==21;
abdomenMask = ~RegionGrowing(abdomenMask,0.5,[1 1]);

upperArmsMask = importedImage(:,:,1)==34 & importedImage(:,:,2)==177 & importedImage(:,:,3)==76;
upperArmsMask = ~RegionGrowing(upperArmsMask,0.5,[1 1]);

lowerArmsMask = importedImage(:,:,1)==181 & importedImage(:,:,2)==230 & importedImage(:,:,3)==29;
lowerArmsMask = ~RegionGrowing(lowerArmsMask,0.5,[1 1]);

handsMask = importedImage(:,:,1)==255 & importedImage(:,:,2)==242 & importedImage(:,:,3)==0;
handsMask = ~RegionGrowing(handsMask,0.5,[1 1]);

blanketMask = importedImage(:,:,1)==63 & importedImage(:,:,2)==72 & importedImage(:,:,3)==204;
blanketMask = ~RegionGrowing(blanketMask,0.5,[1 1]);

mattressMask = importedImage(:,:,1)==163 & importedImage(:,:,2)==73 & importedImage(:,:,3)==164;
mattressMask = ~RegionGrowing(mattressMask,0.5,[1 1]);

cushionMask = importedImage(:,:,1)==153 & importedImage(:,:,2)==217 & importedImage(:,:,3)==234;
cushionMask = ~RegionGrowing(cushionMask,0.5,[1 1]);

floorMask = importedImage(:,:,1)==185 & importedImage(:,:,2)==122 & importedImage(:,:,3)==87;
floorMask = ~RegionGrowing(floorMask,0.5,[1 1]);

wallMask = importedImage(:,:,1)==255 & importedImage(:,:,2)==174 & importedImage(:,:,3)==201;
wallMask = ~RegionGrowing(wallMask,0.5,[1 1]);

% stitch all masks together
labelledImage = ones(size(importedImage,1),size(importedImage,2),'uint8');
labelledImage(headMask) = 2;
labelledImage(abdomenMask) = 3;
labelledImage(upperArmsMask) = 4;
labelledImage(lowerArmsMask) = 5;
labelledImage(handsMask) = 6;
labelledImage(blanketMask) = 7;
labelledImage(mattressMask) = 8;
labelledImage(cushionMask) = 9;
labelledImage(floorMask) = 10;
labelledImage(wallMask) = 11;
%% now do the Slic

% run the import script with user input
sourceFolder = './data';
depthContainer = loadMat(sourceFolder);

numFrames = size(depthContainer,3);

frameInd = indFile;
depthImage = double(depthContainer(:,:,frameInd));
depthImage = depthImage/max(depthImage(:)); %all values should be inside [0,1]
depthImage = cat(3,depthImage,depthImage,depthImage);
tic
[l, Am, Sp, d] = slic(depthImage, 1000, 5, 1.5); % weight 5 gives good results on depth, however with radius it takes 12secs per frame, with radius 1.5 it takes 13.7 secs
toc
figure
imshow(drawregionboundaries(l,depthImage,[255 0 0]))

%% now labels need to be assigned to superpixels
numSp = numel(unique(l));
for indSp = 1:numSp
    
end

%% now a feature needs to be found for each superpixel
for indSp = 1:numSp
    [pcaVecs,ptsInPcaSpace,pcaVars]=pca(ptCloud( da an der richtigen Stelle halt));
end

%% now libsvm is to be fed with features and labels

